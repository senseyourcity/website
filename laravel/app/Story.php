<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Story extends Model {

	use SoftDeletes;
	
	public static $createRules = array(
		'title' => 'required',
		'text' => 'required',
		'lat' => 'required|numeric',
		'lng' => 'required|numeric'
	);
	
	public static $updateRules = array(
		'id' => 'required|integer|exists:stories,id',
		'title' => 'required',
		'text' => 'required'
	);
	
	protected $hidden = ['deleted_at'];
	
	public function user()
    {
        return $this->belongsTo('App\User');
    }
	
	/**
	 * Returns the images of the story.
	 *
	 * @return array
	 */
	public function images()
    {
        return $this->morphMany('App\Image', 'imagable');
    }
	
		/**
	 * Returns the comments on the story.
	 *
	 * @return array
	 */
	public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

}
