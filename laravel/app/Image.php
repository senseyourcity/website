<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Image extends Model {

	use SoftDeletes;
	
	public static $createRules = array(
		'file' => 'image',
		'caption' => 'string',
		'model' => 'required|in:comment,user,story',
		'id' => 'required|integer'
	);
	
	protected $hidden = ['path','name','imagable_id','imagable_type','deleted_at'];
	
	public function imagable()
    {
        return $this->morphTo();
    }
	
	public function user()
    {
        return $this->belongsTo('App\User');
    }

}
