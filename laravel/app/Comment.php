<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model {

	use SoftDeletes;
	
	public static $createRules = array(
		'text' => 'required',
		'model' => 'required|in:station,measurement,comment,image,story',
		'id' => 'required|integer'
	);
	
	/**
	 * Returns the comments on the comment.
	 *
	 * @return array
	 */
	public function commentable()
    {
        return $this->morphTo();
    }
	
	/**
	 * Returns the images of the comment.
	 *
	 * @return array
	 */
	public function images()
    {
        return $this->morphMany('App\Image', 'imagable');
    }
	
	/**
	 * Returns the owner of the comment.
	 *
	 * @return array
	 */
	public function user()
    {
        return $this->belongsTo('App\User');
    }

}
