<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract {

	use Authenticatable, CanResetPassword;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'users';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'email', 'password','params'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['password', 'remember_token'];
	
	/**
	 * Returns the stations owned by the user.
	 *
	 * @return array
	 */
	public function stations()
    {
        return $this->hasMany('App\Station');
    }
	
	/**
	 * Returns the comments made by the user.
	 *
	 * @return array
	 */
	public function comments()
    {
        return $this->hasMany('App\Comment');
    }
	
	/**
	 * Returns the images of the user.
	 *
	 * @return array
	 */
	public function images()
    {
        return $this->hasMany('App\Image');
    }
	
	/**
	 * Returns the profileImage of the user.
	 *
	 * @return array
	 */
	public function profileImage()
    {
        return $this->morphMany('App\Image', 'imagable');
    }
	
	/**
	 * Returns the stories made by the user.
	 *
	 * @return array
	 */
	public function stories()
    {
        return $this->hasMany('App\Story');
    }

}
