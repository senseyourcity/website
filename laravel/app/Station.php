<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Station extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'stations';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name','street','housenumber','zipcode','city','latitude','longitude','token','params'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['housenumber','token'];
	
	/**
	 * Returns the owner of the station.
	 *
	 * @return array
	 */
	public function user()
    {
        return $this->belongsTo('App\User');
    }
	
	/**
	 * Returns the measurements done by the station.
	 *
	 * @return array
	 */
	public function measurements()
    {
        return $this->hasMany('App\Measurement');
    }
	
	/**
	 * Returns the comments on the station.
	 *
	 * @return array
	 */
	public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

}
