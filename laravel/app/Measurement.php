<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Measurement extends Model {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'measurements';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['station_id', 'token', 'temperature', 'humidity', 'lightIntensity', 'windSpeed', 'windDirection', 'particulates', 'carbonMonoxide', 'nitrogenOxides', 'sulfurOxides', 'ammonia'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = ['token'];
	
	/**
	 * Returns the station that did the measurement.
	 *
	 * @return array
	 */
	public function station()
    {
        return $this->belongsTo('App\Station');
    }
	
	/**
	 * Returns the latest measurement in an array of measurements.
	 *
	 * @param  array  $query
	 * @return array
	 */
	public function scopeLatest($query)
    {
        return $query->orderBy('created_at')->first();
    }
	
	/**
	 * Returns the comments on the measurement.
	 *
	 * @return array
	 */
	public function comments()
    {
        return $this->morphMany('App\Comment', 'commentable');
    }

}
