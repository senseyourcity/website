<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// general
Route::get('/', ['middleware' => 'csrf', 'uses' => 'WelcomeController@index']);

// private
Route::group(['middleware' => ['auth', 'csrf']], function()
{
	Route::get('home', ['uses' => 'HomeController@index']);
	
	// Data
	Route::get('data/story', ['uses' => 'DataController@getStories']);
	Route::get('data/station', ['uses' => 'DataController@getStations']);
	Route::get('data/measurement', ['uses' => 'DataController@getMeasurements']);
	Route::get('data/measurement/{id}', ['uses' => 'DataController@getMeasurementsOfStation']);
	Route::get('data/measurement/paginate/{id}', ['uses' => 'DataController@getMeasurementsOfStationPaginated']);
	Route::get('data/latest/measurement/', ['uses' => 'DataController@getLatestMeasurements']);
	
	// Station
	Route::get('stations/search', 'StationsController@search');
	Route::resource('stations', 'StationsController');

	// User
	Route::resource('user', 'UserController', ['except' => ['create', 'store', 'destroy']]);
	
	// Comment
	Route::resource('comment', 'CommentController');
	
	// Image
	Route::resource('image', 'ImageController');
	
	// Story
	Route::resource('story', 'StoryController');
	
	// Compare
	Route::get('compare', ['uses' => 'CompareController@index']);
	Route::get('compare/{left}/{right}', ['uses' => 'CompareController@show']);

});

// api
Route::group(['prefix' => 'api'], function()
{
	
	// general
	Route::get('/', ['uses' => 'Api\ApiController@index']);

	Route::group(['prefix' => 'v1', 'before' => 'oauth'], function()
	{
		//Route::resource('measurement', 'Api\MeasurementController');
		
		// data
		Route::get('station', ['uses' => 'DataController@getStations']);
		Route::get('measurement', ['uses' => 'DataController@getMeasurements']);
		Route::get('measurement/{id}', ['uses' => 'DataController@getMeasurementsOfStation']);
		Route::get('measurement/paginate/{id}', ['uses' => 'DataController@getMeasurementsOfStationPaginated']);
		Route::get('latest/measurement/', ['uses' => 'DataController@getLatestMeasurements']);
	});
});

// authentication
Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

// oauth
Route::get('oauth/authorize', ['before' => 'check-authorization-params|auth', function() {
    // display a form where the user can authorize the client to access it's data
    return view('oauth/authorization-form', Authorizer::getAuthCodeRequestParams());
}]);

Route::post('oauth/authorize', ['before' => 'csrf|check-authorization-params|auth', function() {

	$params['user_id'] = Auth::user()->id;

	$redirectUri = '';

	// if the user has allowed the client to access its data, redirect back to the client with an auth code
	if (Input::get('approve') !== null) {
		$redirectUri = Authorizer::issueAuthCode('user', $params['user_id'], $params);
	}

	// if the user has denied the client to access its data, redirect back to the client with an error message
	if (Input::get('deny') !== null) {
		$redirectUri = Authorizer::authCodeRequestDeniedRedirectUri();
	}

	return Redirect::to($redirectUri);
}]);

Route::post('oauth/access_token', function() {
	return Response::json(Authorizer::issueAccessToken());
});