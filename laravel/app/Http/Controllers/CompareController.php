<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

use App\Station;

class CompareController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return view('compare.index');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($left,$right)
	{
		try {
			return view('compare.show', ['left' => Station::where('token',$left)->firstOrFail(), 'right' => Station::where('token',$right)->firstOrFail()]);
		} catch(ModelNotFoundException $e) {
			return Redirect::to('stations')->with('errorM' , ['Station not found']);
		}
	}

}
