<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Image;
use App\User;

use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;
use Validator;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Database\Eloquent\Model;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Response;
use Storage;

class ImageController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$user = User::find(Auth::id());
		
		return view('image.index', ['images' => $user->images]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('image.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		if ($request->hasFile('image') && $request->file('image')->isValid()){
			
			$validator = Validator::make(
				[
					'caption' => $request->input('caption'),
					'file' => $request->file('image'),
					'model' => $request->input('model'),
					'id' => $request->input('id')
				],
				Image::$createRules
			);
			
			if($validator->passes()){
				$path = storage_path().'\app\upload\\';
				$filename = str_random(32).'_'.time().'.'.$request->file('image')->getClientOriginalExtension();
				$request->file('image')->move($path, $filename);
				
				$image = new Image;
				$image->caption = $request->input('caption');
				$image->path = '\upload\\'.$filename;
				$image->name = $filename;
				$image->user_id = Auth::id();
				
				if ($request->input('model') == 'user') {
					$user = User::find($request->input('id'));
					$user->profileImage()->save($image);
				}
				else if ($request->input('model') == 'comment') {
					$comment = Comment::find($request->input('id'));
					$comment->images()->save($image);
				} else if ($request->input('model') == 'story') {
					$story = Story::find($request->input('id'));
					$story->images()->save($image);
				}
				
				return Redirect::to('image');
			}
			
			$request->flash();
			return Redirect::back()->withErrors($validator);
		}

		$request->flash();
		return Redirect::back();
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$image = Image::find($id);

		return response()->make(Storage::get($image->path))->header('Content-type', Storage::mimeType($image->path));
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
