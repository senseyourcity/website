<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Station;
use App\Comment;
use App\Measurement;
use App\Image;
use App\Story;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;
use Validator;
use Illuminate\Database\Eloquent\Model;

class CommentController extends Controller {
	
	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$validator = Validator::make(
			[
				'text' => $request->input('text'),
				'model' => $request->input('model'),
				'id' => $request->input('id')
			],
			Comment::$createRules
		);
		
		if($validator->passes()){
			$comment = new Comment;
			$comment->text = $request->input('text');
			$comment->user_id = Auth::id();
			
			if ($request->input('model') == 'station') {
				$station = Station::find($request->input('id'));
				$station->comments()->save($comment);
			}
			else if ($request->input('model') == 'measurement') {
				$station = Measurement::find($request->input('id'));
				$station->comments()->save($comment);
			}
			else if ($request->input('model') == 'comment') {
				$station = Comment::find($request->input('id'));
				$station->comments()->save($comment);
			}
			else if ($request->input('model') == 'image') {
				$station = Image::find($request->input('id'));
				$station->comments()->save($comment);
			}
			else if ($request->input('model') == 'story') {
				$station = Story::find($request->input('id'));
				$station->comments()->save($comment);
			}
			
			return Redirect::back();
		}
		
		$request->flash();
		return Redirect::back()->withErrors($validator);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		/*$comment = new Comment;
		try {
			$comment = Comment::findOrFail($id);
		} catch(ModelNotFoundException $e) {
			return Redirect::back()->with('errorM' , ['Comment not found']);
		}
		
		if ($comment->user_id != Auth::id()){
			return Redirect::back()->with('errorM' , ['You do not own this station']);
		}
		
		return view('comment.edit', ['comment' => $comment, 'messages' => null]);*/
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
