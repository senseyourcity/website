<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Station;
use Auth;
use Session;
use Redirect;
use Illuminate\Http\Request;

class UserController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$successM = null;
		if (Session::has('successM'))
		{
			$successM = Session::get('successM');
		}
		
		$warningM = null;
		if (Session::has('warningM'))
		{
			$warningM = Session::get('warningM');
		}
		
		$errorM = null;
		if (Session::has('errorM'))
		{
			$errorM = Session::get('errorM');
		}
		
		$infoM = null;
		if (Session::has('infoM'))
		{
			$infoM = Session::get('infoM');
		}
		
		$user = User::find(Auth::id());
		$user->params = json_decode($user->params);
		return view('user.index', ['user' => $user, 'warningM' => $warningM, 'successM' => $successM, 'errorM' => $errorM, 'infoM' => $infoM]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		if($id == Auth::id()){
			Session::reflash();
			return Redirect::to('user');
		}
		
		$user = new User;
		try {
			$user = User::findOrFail($id);
		} catch(ModelNotFoundException $e) {
			Session::reflash();
			$errorM = null;
			if (Session::has('errorM'))
			{
				$errorM = Session::get('errorM');
			}
			array_push($errorM,'User not found');
			return Redirect::to('user')->with('errorM' , $errorM);
		}
		
		$successM = null;
		if (Session::has('successM'))
		{
			$successM = Session::get('successM');
		}
		
		$warningM = null;
		if (Session::has('warningM'))
		{
			$warningM = Session::get('warningM');
		}
		
		$errorM = null;
		if (Session::has('errorM'))
		{
			$errorM = Session::get('errorM');
		}
		
		$infoM = null;
		if (Session::has('infoM'))
		{
			$infoM = Session::get('infoM');
		}
		
		$user->params = json_decode($user->params);
		return view('user.show', ['user' => $user, 'warningM' => $warningM, 'successM' => $successM, 'errorM' => $errorM, 'infoM' => $infoM]);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$user = new User;
		try {
			$user = User::findOrFail($id);
		} catch(ModelNotFoundException $e) {
			return Redirect::to('user')->with('errorM' , ['User not found']);
		}
		
		if ($user->id != Auth::id()){
			return view('user.show', ['user' => $user, 'errorM' => ['You can only edit your own profile.']]);
		}
		
		return view('user.edit', ['user' => $user, 'messages' => null]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param  Request  $request
	 * @return Response
	 */
	public function update($id, Request  $request)
	{
		$messages = array();
		$errors = false;
		$user = new User;
		
		try {
			$user = User::findOrFail($id);
		} catch(ModelNotFoundException $e) {
			return Redirect::to('user')->with('errorM' , ['User not found']);
		}
		
		if ($user->id != Auth::id()){
			return view('user.show', ['user' => $user, 'errorM' => ['You can only edit your own profile.']]);
		}
		
		if($request->has('name','email')){
			$regexEmail = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
			if (!preg_match($regexEmail, $request->input('email'))){
				$errors = true;
				array_push($messages, 'Please fill in a correct email');
			}
			
			if (!$errors){
				$station->name = $request->input('name');
				$station->street = $request->input('street');
				$station->housenumber = $request->input('housenumber');
				$station->zipcode = $request->input('zipcode');
				$station->city = $request->input('city');
				$station->save();
				
				return Redirect::to('stations')->with('successM', ['The station has been updated!']);
			}
		} else {
			array_push($messages, 'Please fill in all fields');
		}
		
		$request->flash();
		return view('stations.edit', ['station' => $station, 'messages' => $messages]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::to('user')->with('warningM', ['This function is not ready yet.']);
	}

}
