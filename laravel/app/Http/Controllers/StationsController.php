<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Station;
use App\User;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class StationsController extends Controller {
	
	/*
	|--------------------------------------------------------------------------
	| Stations Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders the stations pages
	|
	*/

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$successM = null;
		if (Session::has('successM'))
		{
			$successM = Session::get('successM');
		}
		
		$warningM = null;
		if (Session::has('warningM'))
		{
			$warningM = Session::get('warningM');
		}
		
		$errorM = null;
		if (Session::has('errorM'))
		{
			$errorM = Session::get('errorM');
		}
		
		$infoM = null;
		if (Session::has('infoM'))
		{
			$infoM = Session::get('infoM');
		}
		
		$ownStations = User::find(Auth::id())->stations;
		return view('stations.index', ['stations' => Station::all(), 'ownStations' => $ownStations, 'warningM' => $warningM, 'successM' => $successM, 'errorM' => $errorM, 'infoM' => $infoM]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('stations.create', ['messages' => null]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$messages = array();
		$errors = false;
		
		if($request->has('name','street','housenumber','zipcode','city','token','lat','lng')){
			$regexZip = '~\A[1-9]\d{3} ?[a-zA-Z]{2}\z~';
			if (!preg_match($regexZip, $request->input('zipcode'))){
				$errors = true;
				array_push($messages, 'Please fill in a correct zipcode');
			}
			if (count(Station::where('token', '=', $request->input('token'))->get()) > 0){
				$errors = true;
				array_push($messages, 'This token is already in use');
			}
			
			if (!$errors){
				$station = new Station;
				$station->name = $request->input('name');
				$station->street = $request->input('street');
				$station->housenumber = $request->input('housenumber');
				$station->zipcode = $request->input('zipcode');
				$station->city = $request->input('city');
				$station->token = $request->input('token');
				$station->latitude = $request->input('lat');
				$station->longitude = $request->input('lng');
				$station->user_id = Auth::id();
				$station->save();
				
				return Redirect::to('stations')->with('successM', ['A new station has been added!']);
			}
		} else {
			array_push($messages, 'Please fill in all fields');
		}
		
		$request->flash();
		return view('stations.create', ['messages' => $messages]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try {
			return view('stations.show', ['station' => Station::findOrFail($id)]);
		} catch(ModelNotFoundException $e) {
			return Redirect::to('stations')->with('errorM' , ['Station not found']);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$station = new Station;
		try {
			$station = Station::findOrFail($id);
		} catch(ModelNotFoundException $e) {
			return Redirect::to('stations')->with('errorM' , ['Station not found']);
		}
		
		if ($station->user_id != Auth::id()){
			return view('stations.show', ['station' => $station, 'errorM' => ['You do not own this Station.']]);
		}
		
		return view('stations.edit', ['station' => $station, 'messages' => null]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @param  Request  $request
	 * @return Response
	 */
	public function update($id, Request  $request)
	{
		$messages = array();
		$errors = false;
		$station = new Station;
		
		try {
			$station = Station::findOrFail($id);
		} catch(ModelNotFoundException $e) {
			return Redirect::to('stations')->with('errorM' , ['Station not found']);
		}
		
		if ($station->user_id != Auth::id()){
			return view('stations.show', ['station' => $station, 'errorM' => ['You do not own this Station.']]);
		}
		
		if($request->has('name','street','housenumber','zipcode','city','token')){
			$regexZip = '~\A[1-9]\d{3} ?[a-zA-Z]{2}\z~';
			if (!preg_match($regexZip, $request->input('zipcode'))){
				$errors = true;
				array_push($messages, 'Please fill in a correct zipcode');
			}
			if ($station->token != $request->input('token')){
				$errors = true;
				array_push($messages, 'The tokens do not match');
			}
			
			if (!$errors){
				$station->name = $request->input('name');
				$station->street = $request->input('street');
				$station->housenumber = $request->input('housenumber');
				$station->zipcode = $request->input('zipcode');
				$station->city = $request->input('city');
				$station->save();
				
				return Redirect::to('stations')->with('successM', ['The station has been updated!']);
			}
		} else {
			array_push($messages, 'Please fill in all fields');
		}
		
		$request->flash();
		return view('stations.edit', ['station' => $station, 'messages' => $messages]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		return Redirect::to('stations')->with('warningM', ['This function is not ready yet.']);
	}
	
	/**
	 * search for the specified resource.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function search(Request  $request)
	{
		$stations = Station::where('name', 'LIKE', "%$request->input('q')%")
		->orWhere('name', 'LIKE', "%".$request->input('q')."%")
		->orWhere('street', 'LIKE', "%".$request->input('q')."%")
		->orWhere('zipcode', 'LIKE', "%".$request->input('q')."%")
		->orWhere('city', 'LIKE', "%".$request->input('q')."%")->get();
		
		return view('stations.search', ['stations' => $stations, 'q' => $request->input('q')]);
	}

}
