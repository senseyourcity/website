<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Story;
use App\User;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Auth;
use Redirect;
use Session;
use Validator;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

class StoryController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$ownStories = User::find(Auth::id())->stories;
		return view('story.index', ['stories' => Story::all(), 'ownStories' => $ownStories]);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return view('story.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  Request  $request
	 * @return Response
	 */
	public function store(Request $request)
	{
		$messages = array();
		$errors = false;
		
		$validator = Validator::make(
			[
				'title' => $request->input('title'),
				'text' => $request->input('text'),
				'lat' => $request->input('lat'),
				'lng' => $request->input('lng')
			],
			Story::$createRules
		);
		
		if($validator->passes()){

			$story = new Story;
			$story->title = $request->input('title');
			$story->text = $request->input('text');
			$story->lat = $request->input('lat');
			$story->lng = $request->input('lng');
			$story->user_id = Auth::id();
			$story->save();
			
			return Redirect::to('story')->with('successM', ['A new story has been added!']);
		}
	
		$request->flash();
		return view('story.create', ['messages' => $messages]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		try {
			return view('story.show', ['story' => Story::findOrFail($id)]);
		} catch(ModelNotFoundException $e) {
			return Redirect::to('story')->with('errorM' , ['Story not found']);
		}
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$messages = array();
		
		try {
			return view('story.edit', ['story' => Story::findOrFail($id), 'messages' => $messages]);
		} catch(ModelNotFoundException $e) {
			return Redirect::to('story')->with('errorM' , ['Story not found']);
		}
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, Request $request)
	{
		$messages = array();
		$errors = false;
		
		$validator = Validator::make(
			[
				'id' => $id,
				'title' => $request->input('title'),
				'text' => $request->input('text')
			],
			Story::$updateRules
		);
		
		if($validator->passes()){

			$story = Story::find($id);
			$story->title = $request->input('title');
			$story->text = $request->input('text');
			$story->save();
			
			return Redirect::to('story')->with('successM', ['Story has been updated']);
		}
	
		$request->flash();
		return view('story.ecit', ['messages' => $messages]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
