<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Story;
use App\Station;
use App\Measurement;

class DataController extends Controller {

	
	/**
	 * Return the stories to the user.
	 *
	 * @return Response
	 */
	public function getStories()
	{
		$stories = Story::with('images')->get();
		return response()->json(['success' => true, 'description' => 'All stories', 'data' => $stories, 'timestamp' => time(), 'message' => null]);
	}
	
	/**
	 * Return the stations to the user.
	 *
	 * @return Response
	 */
	public function getStations()
	{
		$stations = Station::all();
		return response()->json(['success' => true, 'description' => 'All stations', 'data' => $stations, 'timestamp' => time(), 'message' => null]);
	}
	
	/**
	 * Return the measurements to the user.
	 *
	 * @return Response
	 */
	public function getMeasurements()
	{
		$measurements = Measurement::all();
		return response()->json(['success' => true, 'description' => 'All measurements', 'data' => $measurements, 'timestamp' => time(), 'message' => null]);
	}
	
	/**
	 * Return the measurements of one station to the user.
	 *
	 * @return Response
	 */
	public function getMeasurementsOfStation($id)
	{
		$station = Station::findOrFail($id);
		$measurements = $station->measurements->reverse()->take(20);
		return response()->json(['success' => true, 'description' => 'All measurements of station', 'data' => $measurements, 'timestamp' => time(), 'message' => null]);
	}
	
	/**
	 * Return the measurements of one station paginated to the user.
	 *
	 * @return Response
	 */
	public function getMeasurementsOfStationPaginated($id)
	{
		$data = Measurement::where('station_id',$id)->orderBy('created_at', 'desc')->paginate(20);
		$data['success'] = true;
		$data['description'] = 'All measurements of station paginated';
		$data['timestamp'] = time();
		$data['message'] = null;
		return response()->json($data);
	}
	
	/**
	 * Return the measurements to the user.
	 *
	 * @return Response
	 */
	public function getLatestMeasurements()
	{
		$measurements = array();
		foreach (Station::all() as $station)
		{
			$data = $station->toArray();
			$data['measurement'] = $station->measurements->last();
			if ($data['measurement'] == null){
				$data['measurement'] = Measurement::all()->last();
			}
			array_push($measurements, $data);
		}
		
		$temperatureMap = array_map(function($details) {
		  return $details['measurement']['temperature'];
		}, $measurements);
		$temperatureMin = min($temperatureMap);
		$temperatureMax = max($temperatureMap);
		
		$humidityMap = array_map(function($details) {
		  return $details['measurement']['humidity'];
		}, $measurements);
		$humidityMin = min($humidityMap);
		$humidityMax = max($humidityMap);
		
		$windSpeedMap = array_map(function($details) {
		  return $details['measurement']['windSpeed'];
		}, $measurements);
		$windSpeedMin = min($windSpeedMap);
		$windSpeedMax = max($windSpeedMap);
		
		return response()->json(['success' => true, 
			'description' => 'All latest measurements', 
			'data' => $measurements, 
			'temperatureRange' => [$temperatureMin,$temperatureMax], 
			'humidityRange' => [$humidityMin,$humidityMax], 
			'windSpeedRange' => [$windSpeedMin,$windSpeedMax], 
			'timestamp' => time(), 
			'message' => null]);
	}
	
}
