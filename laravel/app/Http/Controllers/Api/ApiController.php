<?php namespace App\Http\Controllers\Api;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class ApiController extends Controller {

	/**
	 * Display the welcome message of the api
	 *
	 * @return Response
	 */
	public function index()
	{
		return response()->json(['success' => true, 'description' => 'Sense your city api V1.0', 'data' => 'Welcome to the api! If you see this message everything is working.', 'timestamp' => time(), 'message' => null]);
	}

}
