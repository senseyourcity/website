<?php

use App\Comment;
use Illuminate\Database\Seeder;

class CommentTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('comments')->delete();
		
		Comment::create(array(
			'user_id'     => 1,
			'text'    => 'Wat een mooi weerstation',
			'commentable_id' => 1,
			'commentable_type' => 'App\Station',
		));

	}

}

?>