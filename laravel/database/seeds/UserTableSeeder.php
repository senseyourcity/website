<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('users')->delete();
		
		User::create(array(
			'name'     => 'Admin',
			'email'    => 'rubensmit@live.nl',
			'password' => Hash::make('password'),
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		User::create(array(
			'name'     => 'Theo de Vries',
			'email'    => 'example@example.com',
			'password' => Hash::make('password'),
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
	}

}

?>