<?php

use App\Image;
use Illuminate\Database\Seeder;

class ImageTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('images')->delete();
		
		Image::create(array(
			'user_id'     => 1,
			'path'    => '\upload\AkNHq5iKE3plCnE9WTnQJjwxQqsdrFJY_1426759870.png',
			'name' => 'AkNHq5iKE3plCnE9WTnQJjwxQqsdrFJY_1426759870.png',
			'caption' => 'This is a caption',
			'imagable_id' => 1,
			'imagable_type' => 'App\Story',
		));
		
		Image::create(array(
			'user_id'     => 1,
			'path'    => '\upload\jpvtPJpZ2azRaDH7Xevg3bOivymWtjJs_1426762621.JPG',
			'name' => 'jpvtPJpZ2azRaDH7Xevg3bOivymWtjJs_1426762621.JPG',
			'caption' => 'This is also a caption',
			'imagable_id' => 1,
			'imagable_type' => 'App\Story',
		));


	}

}

?>