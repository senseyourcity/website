<?php

use App\Measurement;
use Illuminate\Database\Seeder;

class MeasurementTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('measurements')->delete();
		
		$perlin = new \NoiseGenerator\PerlinNoise(3000);
		
		for ($n = 0; $n < 10; $n++){
			$station = DB::table('stations')->where('id', $n+1)->first();
			$lat = $station->latitude*10;
			$lng = $station->longitude*10;
			for ($i = 0; $i < 20; $i++){
				Measurement::create(array(
					'station_id' => $n+1,
					'token' => $n+1,
					'temperature' => ($perlin->smoothNoise($lat,$lng,(time()-(60*$i))*1)/2+1)*40-10, //rand(-10,30),
					'humidity' => ($perlin->smoothNoise($lat,$lng,((time()-(60*$i))+1000)*1)/2+1)*100/2, //rand(0,1000)/10,
					'lightIntensity' => ($perlin->smoothNoise($lat,$lng,((time()-(60*$i))+2000)*1)/2+1)*10-3, //rand(0,100)/10,
					'windSpeed' => ($perlin->smoothNoise($lat,$lng,((time()-(60*$i))+3000)*1)/2+1)*100/2, //rand(0,100),
					'windDirection' => ($perlin->smoothNoise($lat,$lng,((time()-(60*$i))+4000)*1)/2+1)*360, //rand(0,360),
					'particulates' => ($perlin->smoothNoise($lat,$lng,((time()-(60*$i))+5000)*1)/2+1)*10-3, //rand(0,100)/10,
					'carbonMonoxide' => ($perlin->smoothNoise($lat,$lng,((time()-(60*$i))+6000)*1)/2+1)*10-3, //rand(0,100)/10,
					'nitrogenOxides' => ($perlin->smoothNoise($lat,$lng,((time()-(60*$i))+7000)*1)/2+1)*10-3, //rand(0,100)/10,
					'sulfurOxides' => ($perlin->smoothNoise($lat,$lng,((time()-(60*$i))+8000)*1)/2+1)*10-3, //rand(0,100)/10,
					'ammonia' => ($perlin->smoothNoise($lat,$lng,((time()-(60*$i))+9000)*1)/2+1)*10-3, //rand(0,100)/10,
				));
			}
		}
	}
	
	public function add()
	{
		$perlin = new \NoiseGenerator\PerlinNoise(3000);
		
		for ($n = 0; $n < 10; $n++){
			$station = DB::table('stations')->where('id', $n+1)->first();
			$lat = $station->latitude*10;
			$lng = $station->longitude*10;

				Measurement::create(array(
					'station_id' => $n+1,
					'token' => $n+1,
					'temperature' => ($perlin->smoothNoise($lat,$lng,time()*1)/2+1)*40-10, //rand(-10,30),
					'humidity' => ($perlin->smoothNoise($lat,$lng,(time()+1000)*1)/2+1)*100, //rand(0,1000)/10,
					'lightIntensity' => ($perlin->smoothNoise($lat,$lng,(time()+2000)*1)/2+1)*10, //rand(0,100)/10,
					'windSpeed' => ($perlin->smoothNoise($lat,$lng,(time()+3000)*1)/2+1)*100, //rand(0,100),
					'windDirection' => ($perlin->smoothNoise($lat,$lng,(time()+4000)*1)/2+1)*360, //rand(0,360),
					'particulates' => ($perlin->smoothNoise($lat,$lng,((time()-(60))+5000)*1)/2+1)*10-3, //rand(0,100)/10,
					'carbonMonoxide' => ($perlin->smoothNoise($lat,$lng,((time()-(60))+6000)*1)/2+1)*10-3, //rand(0,100)/10,
					'nitrogenOxides' => ($perlin->smoothNoise($lat,$lng,((time()-(60))+7000)*1)/2+1)*10-3, //rand(0,100)/10,
					'sulfurOxides' => ($perlin->smoothNoise($lat,$lng,((time()-(60))+8000)*1)/2+1)*10-3, //rand(0,100)/10,
					'ammonia' => ($perlin->smoothNoise($lat,$lng,((time()-(60))+9000)*1)/2+1)*10-3, //rand(0,100)/10,
				));
			
		}
	}

}

?>