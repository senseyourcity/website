<?php

use App\Station;
use Illuminate\Database\Seeder;

class StationTableSeeder extends Seeder
{

	public function run()
	{
		DB::table('stations')->delete();
		Station::create(array(
			'name' => 'Weerstation',
			'street' => 'Geldersestraat',
			'housenumber' => 6,
			'zipcode' => '3812PP',
			'city' => 'Amersfoort',
			'latitude' => 52.1623961,
			'longitude' => 5.3765526,
			'token' => str_random(4),
			'user_id' => 1,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		Station::create(array(
			'name' => 'Weerstation 2',
			'street' => 'Zevenhuizen',
			'housenumber' => 7,
			'zipcode' => '3811CW',
			'city' => 'Amersfoort',
			'latitude' => 52.157075,
			'longitude' => 5.3892707,
			'token' => str_random(4),
			'user_id' => 2,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		Station::create(array(
			'name' => 'Weerstation 3',
			'street' => 'Scheltussingel',
			'housenumber' => 49,
			'zipcode' => '3814BH',
			'city' => 'Amersfoort',
			'latitude' => 52.1600704,
			'longitude' => 5.3956944,
			'token' => str_random(4),
			'user_id' => 2,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		Station::create(array(
			'name' => 'Weerstation 4',
			'street' => 'Vlasakkerweg',
			'housenumber' => 40,
			'zipcode' => '3811MT',
			'city' => 'Amersfoort',
			'latitude' => 52.1529251,
			'longitude' => 5.3798976,
			'token' => str_random(4),
			'user_id' => 2,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		Station::create(array(
			'name' => 'Weerstation 5',
			'street' => 'Kokjesbongerd',
			'housenumber' => 15,
			'zipcode' => '3817AZ',
			'city' => 'Amersfoort',
			'latitude' => 52.1511546,
			'longitude' => 5.3902134,
			'token' => str_random(4),
			'user_id' => 2,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		Station::create(array(
			'name' => 'Weerstation 6',
			'street' => 'De Vlijtstraat',
			'housenumber' => 21,
			'zipcode' => '3816VS',
			'city' => 'Amersfoort',
			'latitude' => 52.157075,
			'longitude' => 5.3892707,
			'token' => str_random(4),
			'user_id' => 2,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		Station::create(array(
			'name' => 'Weerstation 7',
			'street' => 'Hogeweg',
			'housenumber' => 7,
			'zipcode' => '3814AW',
			'city' => 'Amersfoort',
			'latitude' => 52.1577569,
			'longitude' => 5.399105,
			'token' => str_random(4),
			'user_id' => 2,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		Station::create(array(
			'name' => 'Weerstation 8',
			'street' => 'Merelstraat',
			'housenumber' => 43,
			'zipcode' => '3815RE',
			'city' => 'Amersfoort',
			'latitude' => 52.1669004,
			'longitude' => 5.4029893,
			'token' => str_random(4),
			'user_id' => 2,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		Station::create(array(
			'name' => 'Weerstation 9',
			'street' => 'Koning Nobelpad',
			'housenumber' => 32,
			'zipcode' => '3813KK',
			'city' => 'Amersfoort',
			'latitude' => 52.1718807,
			'longitude' => 5.3916621,
			'token' => str_random(4),
			'user_id' => 2,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
		
		Station::create(array(
			'name' => 'Weerstation 10',
			'street' => 'Keerkring',
			'housenumber' => 50,
			'zipcode' => '3813BC',
			'city' => 'Amersfoort',
			'latitude' => 52.1646096,
			'longitude' => 5.3880805,
			'token' => str_random(4),
			'user_id' => 2,
			'params' => '{"picture": "http://placehold.it/32x32"}',
		));
	}

}

?>