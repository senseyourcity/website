<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('stations', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('name');
			$table->string('street');
			$table->integer('housenumber');
			$table->string('zipcode', 6);
			$table->string('city');
			$table->float('latitude',10,8);
			$table->float('longitude',10,8);
			$table->string('token')->unique();
			$table->integer('user_id');
			$table->json('params');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('stations');
	}

}
