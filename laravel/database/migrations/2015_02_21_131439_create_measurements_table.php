<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMeasurementsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('measurements', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('station_id');
			$table->string('token');
			$table->float('temperature');
			$table->float('humidity');
			$table->float('lightIntensity');
			$table->integer('windSpeed');
			$table->integer('windDirection');
			$table->float('particulates')->nullable();
			$table->float('carbonMonoxide')->nullable();
			$table->float('nitrogenOxides')->nullable();
			$table->float('sulfurOxides')->nullable();
			$table->float('ammonia')->nullable();
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('measurements');
	}

}
