@extends('app')

@section('content')
<div class="container no-background">
	<div class="jumbotron">
	  <h1>Be your own weatherman!</h1>
	  <p>Join Sense your City and become your own weatherman. 
	  Want to know if it is raining a few blocks ahead? 
	  Do you want to do your own measurements in your backyard? 
	  Sense your City gives you the opportunity to learn and in the mean time help environmental research into, among other things, the greenhouse effect.</p>
	  <p><a class="btn btn-primary btn-lg" href="/auth/register" role="button">Register</a></p>
	</div>
</div>
@endsection