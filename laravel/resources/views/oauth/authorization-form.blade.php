@extends('app')

@section('content')
<div class="container-fluid firstelementpadding">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="panel panel-default">
				<div class="panel-heading">Authorize {{ $client->getName() }}to access the following</div>
				<div class="panel-body">
					<ul class="list-group">
					@foreach($scopes as $scope)
					  <li class="list-group-item">{{ $scope->getDescription() }}</li>
					@endforeach
					</ul>
					<form class="form-horizontal" role="form" method="POST" action="{!! Request::fullUrl() !!}">
						<input type="hidden" name="_token" value="{{ csrf_token() }}">
						<input class="btn btn-success" type="submit" name="approve" value="Approve">
						<input class="btn btn-danger" type="submit" name="deny" value="Deny">
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
