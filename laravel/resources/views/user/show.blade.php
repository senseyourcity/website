@extends('app')

@section('content')
<div class="container">
	
	<div class="page-header">
	  <h1>{{ $user->name }}
	  </h1>
	</div>
	
	<div class="row">
		<div class="col-md-12">
		
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Profile</h3></div>
				<div class="panel-body">
					<div class="media">
					  <div class="media-left">
						<img class="media-object" src="{{ $user->params->picture }}" alt="Profilepicture">
					  </div>
					  <div class="media-body">
						<h4 class="media-heading">{{ $user->name }}</h4>
					  </div>
					</div>
				</div>
			</div>
			
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Stations</h3></div>
				<div class="panel-body">
					@if (count($user->stations) > 0)
						<ul class="list-group">
						@foreach ($user->stations as $station)
						  <li class="list-group-item"><a href="/stations/{{ $station->id }}">{{ $station->name }}</a></li>
						@endforeach
						</ul>
					@else
						You do not have any stations
					@endif
				</div>
			</div>
			
		</div>
	</div>
	
</div>
@endsection
