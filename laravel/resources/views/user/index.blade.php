@extends('app')

@section('content')
<div class="container">

	<div class="page-header">
	  <h1>Settings
	  </h1>
	</div>
	
	<div class="row">
		<div class="col-md-12">
		
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Profile</h3></div>
				<div class="panel-body">
					<div class="media">
					  <div class="media-left">
						<img class="media-object" src="{{ $user->params->picture }}" alt="Profilepicture">
					  </div>
					  <div class="media-body">
						<h4 class="media-heading">{{ $user->name }}</h4>
							<strong>Email:</strong> {{ $user->email }}<br>
					  </div>
					</div>
					<span class="btn-group pull-right" role="group">
					  <a href="/user/{{ $user->id }}/edit"><button type="button" class="btn btn-default">Edit profile</button></a>
					</span>
				</div>
			</div>

			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Security</h3></div>
				<div class="panel-body">
					<div class="btn-group" role="group">
					  <a href="/auth/password"><button type="button" class="btn btn-default">Change password</button></a>
					</div>
				</div>
			</div>
		
			<div class="panel panel-default">
				<div class="panel-heading"><h3 class="panel-title">Stations</h3></div>
				<div class="panel-body">
					@if (count($user->stations) > 0)
						<ul class="list-group">
						@foreach ($user->stations as $station)
						  <li class="list-group-item"><a href="/stations/{{ $station->id }}">{{ $station->name }}</a></li>
						@endforeach
						</ul>
					@else
						This user does not have any weatherstations
					@endif
				</div>
			</div>
			
		</div>
	</div>
	
</div>
@endsection
