@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><h2 class="panel-title">Edit Profile</h2></div>
				<div class="panel-body">
					@if (count($messages) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($messages as $message)
									<li>{{ $message }}</li>
								@endforeach
							</ul>
						</div>
					
					<form class="form-horizontal" role="form" method="POST" action="/user/{{ $user->id }}">
					  <div class="form-group">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ old('name') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
						  <input type="email" class="form-control" id="email" placeholder="example@example.com" name="email" value="{{ old('email') }}" required>
						</div>
					  </div>
					  
					  @else
					<form class="form-horizontal" role="form" method="POST" action="/user/{{ $user->id }}">
					  <div class="form-group">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ $user->name }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="email" class="col-sm-2 control-label">Email</label>
						<div class="col-sm-10">
						  <input type="email" class="form-control" id="email" placeholder="example@example.com" name="email" value="{{ $user->email }}" required>
						</div>
					  </div>
						  
					  @endif
					  
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <input type="hidden" name="_method" value="PUT">
					  <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10" role="group">
						  <button type="submit" class="btn btn-primary">Save</button> <a href="/user"><button type="button" class="btn btn-default">Cancel</button></a>
						</div>
					  </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
