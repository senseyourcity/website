@extends('app')

@section('content')
<div class="container">

	<div class="page-header">
	  <h1>Stations matching {{ $q }}
	  <form class="form-inline pull-right" action="/stations/search" method="get">
		<div class="input-group">
		  <input type="search" name="q" class="form-control" value="{{ $q }}" autofocus>
		  <span class="input-group-btn">
			<button class="btn btn-default" type="submit">
			  Search
			</button>
		  </span>
		</div>
	  </form>
	  </h1>
	</div>
	
	@if(count($stations)>0)
	<div class="row">
		@foreach ($stations as $station)
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="/stations/{{ $station->id }}"><h3 class="panel-title">{{ $station->name }}</h3></a></div>
				<div class="panel-body">
					Location: {{ $station->street }} {{ $station->city }}<br>
					Temperature <span class="badge">20&deg;C</span>
				</div>
			</div>
		</div>
		@endforeach
	</div>
	@else
		<div class="alert alert-info" role="alert"><strong>No stations found!</strong> Try widening your search.</div>
	@endif
	
</div>
@endsection
