@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><h2 class="panel-title">Edit Station</h2></div>
				<div class="panel-body">
					@if (count($messages) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($messages as $message)
									<li>{{ $message }}</li>
								@endforeach
							</ul>
						</div>
					
					<form class="form-horizontal" role="form" method="POST" action="/stations/{{ $station->id }}">
					  <div class="form-group">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ old('name') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="street" class="col-sm-2 control-label">Street</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="street" placeholder="Street" name="street" value="{{ old('street') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="housenumber" class="col-sm-2 control-label">Housenumber</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="housenumber" placeholder="Housenumber" name="housenumber" value="{{ old('housenumber') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="zipcode" class="col-sm-2 control-label">Zipcode</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="zipcode" maxlength="6" placeholder="1234AB" name="zipcode" value="{{ old('zipcode') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="city" class="col-sm-2 control-label">City</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="city" placeholder="City" name="city" value="{{ old('city') }}" required>
						</div>
					  </div>
					  
					  @else
					<form class="form-horizontal" role="form" method="POST" action="/stations/{{ $station->id }}">
					  <div class="form-group">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ $station->name }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="street" class="col-sm-2 control-label">Street</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="street" placeholder="Street" name="street" value="{{ $station->street }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="housenumber" class="col-sm-2 control-label">Housenumber</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="housenumber" placeholder="Housenumber" name="housenumber" value="{{ $station->housenumber }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="zipcode" class="col-sm-2 control-label">Zipcode</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="zipcode" maxlength="6" placeholder="1234AB" name="zipcode" value="{{ $station->zipcode }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="city" class="col-sm-2 control-label">City</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="city" placeholder="City" name="city" value="{{ $station->city }}" required>
						</div>
					  </div>
						  
					  @endif
					  <div class="form-group">
						<label for="token" class="col-sm-2 control-label">Token</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="disabledToken" placeholder="Token" aria-describedby="helpBlock" name="token" value="{{ $station->token }}" disabled>
						  <span id="helpBlock" class="help-block">You can find this token on the label attached to your station</span>
						</div>
					  </div>
					  <input type="hidden" name="token" value="{{ $station->token }}">
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <input type="hidden" name="_method" value="PUT">
					  <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10" role="group">
						  <button type="submit" class="btn btn-primary">Save</button> <a href="/stations/{{ $station->id }}"><button type="button" class="btn btn-default">Cancel</button></a>
						</div>
					  </div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
