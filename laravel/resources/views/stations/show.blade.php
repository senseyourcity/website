@extends('app')

@section('includes')
<script src="/js/Chart.js"></script>

<script>
$( document ).ready(function(){
	var ctx = document.getElementById("myChart").getContext("2d");
	Chart.defaults.global.maintainAspectRatio = false;
	Chart.defaults.global.responsive = true;
	Chart.defaults.global.scaleBeginAtZero = true;
	
	var timestamps = new Array();
	var temperature = new Array();
	var humidity = new Array();
	var lightIntensity = new Array();
	var windSpeed = new Array();
	var particulates = new Array();
	var carbonMonoxide = new Array();
	var nitrogenOxides = new Array();
	var sulfurOxides = new Array();
	var ammonia = new Array();
	
	var loadUrl = "/data/measurement/paginate/{{ $station->id }}";
	var nextUrl = null;
	var prevUrl = null;
		
	var value = {
		labels: timestamps,
		datasets: [
			{
				label: "Temperature",
				fillColor: "rgba(220,220,220,0.2)",
				strokeColor: "rgba(220,220,220,1)",
				pointColor: "rgba(220,220,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,220,220,1)",
				data: temperature
			}
		]
	};
	
	function getData(url) {
		if (url == null){
			url = loadUrl;
		}
		$.getJSON( url, function( data ) {
			nextUrl = data.next_page_url;
			prevUrl = data.prev_page_url;
			
			$.each( data.data, function( key, object ) {
				if (object != null){
					if (object.created_at != null){
						timestamps.push(object.created_at);
						temperature.push(object.temperature);
						humidity.push(object.humidity);
						lightIntensity.push(object.lightIntensity);
						windSpeed.push(object.windSpeed);
						particulates.push(object.particulates);
						carbonMonoxide.push(object.carbonMonoxide);
						nitrogenOxides.push(object.nitrogenOxides);
						sulfurOxides.push(object.sulfurOxides);
						ammonia.push(object.ammonia);
				}}
			});
			value.lables = timestamps.reverse();
			value.datasets[0].data = temperature.reverse();
			var myLineChart = new Chart(ctx).Line(value);
			
			$( ".graph-btn" ).click(function() {
				var type = $(this).attr("value");
				var newData = new Array();
				if (type == 'temperature'){
					newData = temperature.reverse();
				} else if (type == 'humidity') {
					newData = humidity.reverse();
				} else if (type == 'lightIntensity') {
					newData = lightIntensity.reverse();
				} else if (type == 'particulates') {
					newData = particulates.reverse();
				} else if (type == 'carbonMonoxide') {
					newData = carbonMonoxide.reverse();
				} else if (type == 'nitrogenOxides') {
					newData = nitrogenOxides.reverse();
				} else if (type == 'sulfurOxides') {
					newData = sulfurOxides.reverse();
				} else if (type == 'ammonia') {
					newData = ammonia.reverse();
				} else {
					newData = windSpeed.reverse();
				}
				$( ".graph-btn" ).removeClass('btn-primary');
				$(this).addClass('btn-primary');
				for (var i = 0; i<humidity.length; i++){
					myLineChart.datasets[0].points[i].value = newData[i];
				}
				myLineChart.update();
			});
		});
	}
	
	getData(loadUrl);
	
	$( ".next-btn" ).click(function() {
		timestamps = new Array();
		temperature = new Array();
		humidity = new Array();
		lightIntensity = new Array();
		windSpeed = new Array();
		particulates = new Array();
		carbonMonoxide = new Array();
		nitrogenOxides = new Array();
		sulfurOxides = new Array();
		ammonia = new Array();
		getData(prevUrl);
	});
	
	$( ".prev-btn" ).click(function() {
		timestamps = new Array();
		temperature = new Array();
		humidity = new Array();
		lightIntensity = new Array();
		windSpeed = new Array();
		particulates = new Array();
		carbonMonoxide = new Array();
		nitrogenOxides = new Array();
		sulfurOxides = new Array();
		ammonia = new Array();
		getData(nextUrl);
	});
});
</script>

@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading">
				  <h2 class="panel-title">{{ $station->name }}</h2>
				</div>
				<div class="panel-body">
					Location: {{ $station->street }} {{ $station->city }}<br>
					@if (count($station->measurements) > 0)
						Temperature <span class="badge">{{ $station->measurements->last()->temperature }}&deg;C</span>
						Humidity <span class="badge">{{ $station->measurements->last()->humidity }}&#37;</span>
						Light intensity <span class="badge">{{ $station->measurements->last()->lightIntensity }}&#37;</span>
						Wind speed <span class="badge">{{ $station->measurements->last()->windSpeed }}kmh</span>
						Wind direction <span class="badge">{{ $station->measurements->last()->windDirection }}&deg;</span>
						Particulates <span class="badge">{{ $station->measurements->last()->particulates }}&#37;</span>
						Carbon Monoxide <span class="badge">{{ $station->measurements->last()->carbonMonoxide }}&#37;</span>
						Nitrogen Oxides <span class="badge">{{ $station->measurements->last()->nitrogenOxides }}&#37;</span>
						Sulfur Oxides <span class="badge">{{ $station->measurements->last()->sulfurOxides }}&#37;</span>
						Ammonia <span class="badge">{{ $station->measurements->last()->ammonia }}&#37;</span>
					@endif
						
					
					@if ($station->user_id != Auth::id())
					<div class="media"><a href="/user/{{ $station->user_id }}">
					  <div class="media-left">
						<img class="media-object" src="{{ json_decode($station->user->params)->picture }}" alt="Profilepicture">
					  </div>
					  <div class="media-body">
						<h4 class="media-heading">{{ $station->user->name }} <small>Owner</small></h4>
					  </div>
					</a></div>
					@endif
					
					@if ($station->user_id == Auth::id())
					<span class="btn-group pull-right" role="group">
					  <a href="/stations/{{ $station->id }}/edit"><button type="button" class="btn btn-default">Edit</button></a>
					</span>
					@endif
				</div>
				
				<ul class="list-group">
				@if (count($station->comments) > 0)
					@foreach ($station->comments as $comment)
					<li class="list-group-item"><strong>{{ $comment->user->name }}:</strong> {{ $comment->text }} <small class="pull-right text-muted">{{ $comment->updated_at }}</small></li>
					@endforeach
				@endif
					<li class="list-group-item">
						<form class="form-horizontal" role="form" method="POST" action="/comment">
						  <div class="form-group">
							<div class="col-sm-10 col-xs-8">
							  <input type="text" class="form-control" id="text" placeholder="Add comment" name="text" value="{{ old('text') }}" required>
							</div>
							<div class="col-sm-2 col-xs-4">
							  <button type="submit" id="submit" class="btn btn-primary">Comment</button>
							</div>
						  </div>
						  <input type="hidden" name="_token" value="{{ csrf_token() }}">
						  <input type="hidden" name="model" id="model" value="station">
						  <input type="hidden" name="id" id="id" value="{{ $station->id }}">
						</form>
					</li>
				</ul>
				
			</div>
			
			<div class="panel panel-default">
				<div class="panel-heading">
				  <h2 class="panel-title">Graphs</h2>
				</div>
				<div class="panel-body">
					<div><canvas id="myChart" height="350px" width="100%"></canvas></div>
					<div style="text-align:center;">
						<button type="button" class="btn btn-default prev-btn"><span class="glyphicon glyphicon-chevron-left"></span> Previous</button>
						<div class="btn-group" role="group">
						  <button type="button" class="btn btn-default graph-btn btn-primary" value="temperature">Temperature</button>
						  <button type="button" class="btn btn-default graph-btn" value="humidity">Humidity</button>
						  <button type="button" class="btn btn-default graph-btn" value="lightIntensity">Light intensity</button>
						  <button type="button" class="btn btn-default graph-btn" value="windSpeed">Wind speed</button>
						  <button type="button" class="btn btn-default graph-btn" value="particulates">Particulates</button>
						  <button type="button" class="btn btn-default graph-btn" value="carbonMonoxide">Carbon Monoxide</button>
						  <button type="button" class="btn btn-default graph-btn" value="sulfurOxides">Sulfur Oxides</button>
						  <button type="button" class="btn btn-default graph-btn" value="ammonia">Ammonia</button>
						</div>	
						<button type="button" class="btn btn-default next-btn">Next <span class="glyphicon glyphicon-chevron-right"></span></button>
					</div>
				</div>
			</div>
			
			<div class="panel panel-default">
				<div class="panel-heading">
				  <h2 class="panel-title">Measurements by {{ $station->name }}</h2>
				</div>
				<div class="panel-body">
				
				@if (count($station->measurements) > 0)
				  <div class="table-responsive">
					<table class="table">
					  <tr>
						<th>Time</th>
						<th>Temperature</th>
						<th>Humidity</th>
						<th>Light intensity</th>
						<th>Wind speed</th>
						<th>Wind direction</th>
						<th>Particulates</th>
						<th>Carbon Monoxide</th>
						<th>Nitrogen Oxides</th>
						<th>Sulfur Oxides</th>
						<th>Ammonia</th>
					  </tr>
					  @foreach ($station->measurements->reverse()->take(20) as $measurement)
					  <tr>
						<td>{{ $measurement->created_at }}</td>
						<td>{{ $measurement->temperature }}</td>
						<td>{{ $measurement->humidity }}</td>
						<td>{{ $measurement->lightIntensity }}</td>
						<td>{{ $measurement->windSpeed }}</td>
						<td>{{ $measurement->windDirection }}</td>
						<td>{{ $measurement->particulates }}</td>
						<td>{{ $measurement->carbonMonoxide }}</td>
						<td>{{ $measurement->nitrogenOxides }}</td>
						<td>{{ $measurement->sulfurOxides }}</td>
						<td>{{ $measurement->ammonia }}</td>
					  </tr>
					  @endforeach
					</table>
				  </div>
				@else
					This station has no measurements
				@endif
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection
