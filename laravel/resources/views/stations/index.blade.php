@extends('app')

@section('content')
<div class="container">

	<div class="page-header">
	  <h1>Your stations
	  <div class="btn-group pull-right" role="group">
		<a href="/stations/create"><button type="button" class="btn btn-default">Add new station</button></a>
	  </div>
	  </h1>
	</div>
	
	@if (count($ownStations) > 0)
	<div class="row">
		@foreach ($ownStations as $station)
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="/stations/{{ $station->id }}"><h3 class="panel-title">{{ $station->name }}</h3></a></div>
				<div class="panel-body">
					Location: {{ $station->street }} {{ $station->city }}<br>
					@if (count($station->measurements) > 0)
						Temperature <span class="badge">{{ $station->measurements->last()->temperature }}&deg;C</span>
						Humidity <span class="badge">{{ $station->measurements->last()->humidity }}&#37;</span>
						Light intensity <span class="badge">{{ $station->measurements->last()->lightIntensity }}&#37;</span>
						Wind speed <span class="badge">{{ $station->measurements->last()->windSpeed }}kmh</span>
						Wind direction <span class="badge">{{ $station->measurements->last()->windDirection }}&deg;</span>
					@else
						No measurements available
					@endif
				</div>
			</div>
		</div>
		@endforeach
	</div>
	@else
	<div class="alert alert-info" role="alert"><strong>You do not have any weather stations yet!</strong> You can add one by pressing the &quot;Add new station&quot; button above.</div>
	@endif
	
	<div class="page-header">
	  <h1>All stations
	  <form class="form-inline pull-right" action="/stations/search" method="get">
		<div class="input-group">
		  <input type="search" name="q" class="form-control" autofocus>
		  <span class="input-group-btn">
			<button class="btn btn-default" type="submit">
			  Search
			</button>
		  </span>
		</div>
	  </form>
	  </h1>
	</div>
	
	@if (count($stations) > 0)
	<div class="row">
		@foreach ($stations as $station)
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading"><a href="/stations/{{ $station->id }}"><h3 class="panel-title">{{ $station->name }}</h3></a></div>
				<div class="panel-body">
					Location: {{ $station->street }} {{ $station->city }}<br>
					@if (count($station->measurements) > 0)
						Temperature <span class="badge">{{ $station->measurements->last()->temperature }}&deg;C</span>
						Humidity <span class="badge">{{ $station->measurements->last()->humidity }}&#37;</span>
						Light intensity <span class="badge">{{ $station->measurements->last()->lightIntensity }}&#37;</span>
						Wind speed <span class="badge">{{ $station->measurements->last()->windSpeed }}kmh</span>
						Wind direction <span class="badge">{{ $station->measurements->last()->windDirection }}&deg;</span>
					@else
						No measurements available
					@endif
				</div>
			</div>
		</div>
		@endforeach
	</div>
	@else
		<div class="alert alert-info" role="alert"><strong>No stations found!</strong> That's a pitty. You can add one by pressing the &quot;Add new station&quot; button above.</div>
	@endif
	
</div>
@endsection
