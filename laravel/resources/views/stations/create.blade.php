@extends('app')

@section('includes')
<!-- OpenLayers -->
<script type="text/javascript" src="/js/OpenLayers.js"></script>
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
		
			<div class="panel panel-default">
				<div class="panel-heading"><h2 class="panel-title">Add new Station</h2></div>
				<div class="panel-body">
					@if (count($messages) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($messages as $message)
									<li>{{ $message }}</li>
								@endforeach
							</ul>
						</div>
					@endif
					<form class="form-horizontal" role="form" method="POST" action="/stations">
					  <div class="form-group">
						<label for="name" class="col-sm-2 control-label">Name</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="name" placeholder="Name" name="name" value="{{ old('name') }}" required>
						</div>
					  </div>
					  
					  <h5 class="col-sm-offset-2 col-sm-10">Location of the Station</h5>
					  
					  <div class="form-group">
						<label for="street" class="col-sm-2 control-label">Street</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="street" placeholder="Street" name="street" value="{{ old('street') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="housenumber" class="col-sm-2 control-label">Housenumber</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="housenumber" placeholder="Housenumber" name="housenumber" value="{{ old('housenumber') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="zipcode" class="col-sm-2 control-label">Zipcode</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="zipcode" maxlength="6" placeholder="1234AB" name="zipcode" value="{{ old('zipcode') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="city" class="col-sm-2 control-label">City</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="city" placeholder="City" name="city" value="{{ old('city') }}" required>
						</div>
					  </div>
					  
					  <div class="form-group">
						<div class="col-sm-2">
						  <button type="button" id="locate" class="btn btn-primary pull-right">Locate</button>
						</div>
						<div class="col-sm-10">
						  <div id="map" class="map" style="width: 100%; height: 300px;"></div>
						</div>
					  </div>
					  
					  <br>
					  
					  <script>
					  
$( document ).ready(function() {

var map = new OpenLayers.Map('map');
var layer = new OpenLayers.Layer.OSM( "Simple OSM Map");
var markers = new OpenLayers.Layer.Markers( "Markers" );
var marker = new OpenLayers.Marker(new OpenLayers.LonLat(0,0).transform(
        new OpenLayers.Projection("EPSG:4326"),
        map.getProjectionObject()),icon);
map.addLayers([layer, markers]);

var size = new OpenLayers.Size(25,25);
var offset = new OpenLayers.Pixel(-(size.w/2), -size.h);
var icon = new OpenLayers.Icon('/images/marker.png',size,offset);

map.setCenter(
    new OpenLayers.LonLat(5.3965879493517, 52.1637739).transform(
        new OpenLayers.Projection("EPSG:4326"),
        map.getProjectionObject()
    ), 12
);

function update(e) {
	map.setCenter(
		new OpenLayers.LonLat(e.lng, e.lat).transform(
			new OpenLayers.Projection("EPSG:4326"),
			map.getProjectionObject()
		), 17
	);
	markers.removeMarker(marker);
	marker = new OpenLayers.Marker(new OpenLayers.LonLat(e.lng,e.lat).transform(
        new OpenLayers.Projection("EPSG:4326"),
        map.getProjectionObject()),icon)
	markers.addMarker(marker);
}

document.getElementById('locate').onclick = function() {
	$.getJSON( "http://api.opencagedata.com/geocode/v1/json?query="+encodeURI($("#street").val())+"+"+encodeURI($("#housenumber").val())+"+"+encodeURI($("#city").val())+"&pretty=1&key=1c17e2b9c894535e3b9ffe757208250b" )
		.done(function( json ) {
			console.log( json );
			var loc = [];
			loc.lat = json.results[0].geometry.lat;
			loc.lng = json.results[0].geometry.lng;
			update(loc);
			$('#submit').removeAttr("disabled");
			$('#lat').val(loc.lat);
			$('#lng').val(loc.lng);
		})
		.fail(function( jqxhr, textStatus, error ) {
			var err = textStatus + ", " + error;
			console.log( "Request Failed: " + err );
	});
};

});

					  </script>
					  
					  <div class="form-group">
						<label for="token" class="col-sm-2 control-label">Token</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="token" placeholder="Token" aria-describedby="helpBlock" name="token" value="{{ old('token') }}" required>
						  <span id="helpBlock" class="help-block">You can find this token on the label attached to your station</span>
						</div>
					  </div>
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <input type="hidden" name="lat" id="lat" value="0">
					  <input type="hidden" name="lng" id="lng" value="0">
					  <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						  <button type="submit" id="submit" class="btn btn-primary" disabled>Add</button> <a href="/stations"><button type="button" class="btn btn-default">Cancel</button></a>
						</div>
					  </div>
					</form>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection
