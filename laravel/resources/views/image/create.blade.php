@extends('app')

@section('content')
<div class="container">

	<div class="row">
	  <div class="col-md-12">
		<div class="panel panel-default">
		  <div class="panel-heading">
			<h1 class="panel-title">Upload Image</h1>
		  </div>
		  <div class="panel-body">

			<form class="form-horizontal" role="form" method="POST" action="/image" enctype="multipart/form-data">
			
			  <input type="hidden" name="_token" value="{{ csrf_token() }}">
			  
			  <div class="form-group">
				<label for="image" class="col-sm-2 control-label">Image</label>
				<div class="col-sm-10">
				  <input type="file" id="image" placeholder="Image" name="image" accept="image/*" required>
				  <p class="help-block">Only images allowed in the formats jpeg,bmp and png</p>
				</div>
			  </div>
			  
			  <div class="form-group">
				<label for="caption" class="col-sm-2 control-label">Caption</label>
				<div class="col-sm-10">
				  <input type="text" class="form-control" id="caption" placeholder="Caption" name="caption" value="{{ old('caption') }}">
				</div>
			  </div>

			  <input type="hidden" name="model" value="user">
			  <input type="hidden" name="id" value="{{ Auth::id() }}">
			  <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
				  <button type="submit" id="submit" class="btn btn-primary">Upload</button> <a href="/image"><button type="button" class="btn btn-default">Cancel</button></a>
				</div>
			  </div>
			</form>
		  </div>
		</div>
	  </div>
	</div>
	
</div>
@endsection
