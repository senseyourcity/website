@extends('app')

@section('content')
<div class="container">

	<div class="page-header">
	  <h1>Your Images
	  <div class="btn-group pull-right" role="group">
		<a href="/image/create"><button type="button" class="btn btn-default">Upload new image</button></a>
	  </div>
	  </h1>
	</div>
	
	@if (count($images) > 0)
	<div class="row">
		@foreach ($images as $image)
			<div class="col-sm-2"><img src="image/{{ $image->id }}" class="img-responsive" alt="{{ $image->caption }}"></div>
		@endforeach
	</div>
	@else
	<div class="alert alert-info" role="alert"><strong>You do not have any images yet!</strong> You can add one by pressing the &quot;Upload new image&quot; button above.</div>
	@endif
	
</div>
@endsection
