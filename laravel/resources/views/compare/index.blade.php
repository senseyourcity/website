@extends('app')

@section('includes')
<script>
$( document ).ready(function(){
	$( "#compare" ).click(function() {
		var left = $("#left").val();
		var right = $("#right").val();
		window.location.href = '/compare/'+left+'/'+right;
	});
});
</script>
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<div class="jumbotron text-center">
			  <h1>Compare your stations</h1>
			  <br>
			  <div>
				<div class="form-group col-md-offset-2 col-md-4">
					<input type="text" class="form-control input-lg" id="left" placeholder="Token">
				</div>
				<div class="form-group col-md-4">
					<input type="text" class="form-control input-lg" id="right" placeholder="Token">
				</div>
			  </div>
			  <br><br><br><br>
			  <div>
			  	<button class="btn btn-primary btn-lg" id="compare">Compare</button>
			  </div>
			  <br>
			</div>
			
		</div>
	</div>
</div>
@endsection
