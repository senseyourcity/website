@extends('app')

@section('includes')
<script src="/js/Chart.js"></script>

<script>
$( document ).ready(function(){
	var ctx = document.getElementById("myChart").getContext("2d");
	Chart.defaults.global.maintainAspectRatio = false;
	Chart.defaults.global.responsive = true;
	Chart.defaults.global.scaleBeginAtZero = true;
	
	var timestamps = new Array();
	var temperatureL = new Array();
	var humidityL = new Array();
	var lightIntensityL = new Array();
	var windSpeedL = new Array();
	var particulatesL = new Array();
	var carbonMonoxideL = new Array();
	var nitrogenOxidesL = new Array();
	var sulfurOxidesL = new Array();
	var ammoniaL = new Array();
	
	var temperatureR = new Array();
	var humidityR = new Array();
	var lightIntensityR = new Array();
	var windSpeedR = new Array();
	var particulatesR = new Array();
	var carbonMonoxideR = new Array();
	var nitrogenOxidesR = new Array();
	var sulfurOxidesR = new Array();
	var ammoniaR = new Array();
	
	var loadUrlL = "/data/measurement/paginate/{{ $left['id'] }}";
	var nextUrlL = null;
	var prevUrlL = null;
	
	var loadUrlR = "/data/measurement/paginate/{{ $right['id'] }}";
	var nextUrlR = null;
	var prevUrlR = null;
		
	var value = {
		labels: timestamps,
		datasets: [
			{
				label: "{{$left['name']}}",
				fillColor: "rgba(220,0,0,0.05)",
				strokeColor: "rgba(220,0,0,1)",
				pointColor: "rgba(220,0,0,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(220,0,0,1)",
				data: temperatureL
			},
			{
				label: "{{$right['name']}}",
				fillColor: "rgba(0,0,220,0.05)",
				strokeColor: "rgba(0,0,220,1)",
				pointColor: "rgba(0,0,220,1)",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#fff",
				pointHighlightStroke: "rgba(0,0,220,1)",
				data: temperatureL
			}
		]
	};
	
	function getData(urlL,urlR) {
		if (urlL == null){
			urlL = loadUrlL;
		}
		if (urlR == null){
			urlR = loadUrlR;
		}
		$.getJSON( urlL, function( data ) {
			nextUrlL = data.next_page_url;
			prevUrlL = data.prev_page_url;
			
			$.each( data.data, function( key, object ) {
				if (object != null){
					if (object.created_at != null){
						timestamps.push(object.created_at);
						temperatureL.push(object.temperature);
						humidityL.push(object.humidity);
						lightIntensityL.push(object.lightIntensity);
						windSpeedL.push(object.windSpeed);
						particulatesL.push(object.particulates);
						carbonMonoxideL.push(object.carbonMonoxide);
						nitrogenOxidesL.push(object.nitrogenOxides);
						sulfurOxidesL.push(object.sulfurOxides);
						ammoniaL.push(object.ammonia);
				}}
			});
			
			value.lables = timestamps.reverse();
			value.datasets[0].data = temperatureL.reverse();
			
			$.getJSON( urlR, function( data ) {
				nextUrlR = data.next_page_url;
				prevUrlR = data.prev_page_url;
				
				$.each( data.data, function( key, object ) {
					if (object != null){
						if (object.created_at != null){
							temperatureR.push(object.temperature);
							humidityR.push(object.humidity);
							lightIntensityR.push(object.lightIntensity);
							windSpeedR.push(object.windSpeed);
							particulatesR.push(object.particulates);
							carbonMonoxideR.push(object.carbonMonoxide);
							nitrogenOxidesR.push(object.nitrogenOxides);
							sulfurOxidesR.push(object.sulfurOxides);
							ammoniaR.push(object.ammonia);
					}}
				});
				
				value.lables = timestamps.reverse();
				value.datasets[1].data = temperatureR.reverse();
				var myLineChart = new Chart(ctx).Line(value, {
					legendTemplate : "<ul style=\"list-style-type: none;\"class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><div style=\"display: inline-block; width: 1em; height: 1em; background-color:<%=datasets[i].strokeColor%>\"></div> <%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>"
				});
				
				update($( ".graph-btn.btn-primary" ).get( 0 ));
				
				$( ".graph-btn" ).click(function() {
					update(this);
				});
				
				function update(origin){
					var type = $(origin).attr("value");
					var newDataL = new Array();
					var newDataR = new Array();
					if (type == 'temperature'){
						newDataL = temperatureL.reverse();
						newDataR = temperatureR.reverse();
					} else if (type == 'humidity') {
						newDataL = humidityL.reverse();
						newDataR = humidityR.reverse();
					} else if (type == 'lightIntensity') {
						newDataL = lightIntensityL.reverse();
						newDataR = lightIntensityR.reverse();
					} else if (type == 'particulates') {
						newDataL = particulatesL.reverse();
						newDataR = particulatesR.reverse();
					} else if (type == 'carbonMonoxide') {
						newDataL = carbonMonoxideL.reverse();
						newDataR = carbonMonoxideR.reverse();
					} else if (type == 'nitrogenOxides') {
						newDataL = nitrogenOxidesL.reverse();
						newDataR = nitrogenOxidesR.reverse();
					} else if (type == 'sulfurOxides') {
						newDataL = sulfurOxidesL.reverse();
						newDataR = sulfurOxidesR.reverse();
					} else if (type == 'ammonia') {
						newDataL = ammoniaL.reverse();
						newDataR = ammoniaR.reverse();
					} else {
						newDataL = windSpeedL.reverse();
						newDataR = windSpeedR.reverse();
					}
					$( ".graph-btn" ).removeClass('btn-primary');
					$(origin).addClass('btn-primary');
					for (var i = 0; i<timestamps.length; i++){
						myLineChart.datasets[0].points[i].value = newDataL[i];
						myLineChart.datasets[1].points[i].value = newDataR[i];
					}
					myLineChart.update();
					
					$( ".chart-legend" ).html(myLineChart.generateLegend());
				}
			});
		});
	}
	
	getData(loadUrlL,loadUrlR);
	
	$( ".next-btn" ).click(function() {
		timestamps = new Array();
		temperature = new Array();
		humidity = new Array();
		lightIntensity = new Array();
		windSpeed = new Array();
		particulates = new Array();
		carbonMonoxide = new Array();
		nitrogenOxides = new Array();
		sulfurOxides = new Array();
		ammonia = new Array();
		getData(prevUrlL,prevUrlR);
	});
	
	$( ".prev-btn" ).click(function() {
		timestamps = new Array();
		temperature = new Array();
		humidity = new Array();
		lightIntensity = new Array();
		windSpeed = new Array();
		particulates = new Array();
		carbonMonoxide = new Array();
		nitrogenOxides = new Array();
		sulfurOxides = new Array();
		ammonia = new Array();
		getData(nextUrlL,nextUrlR);
	});
});
</script>

@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading">
				  <h2 class="panel-title">{{ $left['name'] }} VS. {{ $right['name'] }}</h2>
				</div>
				<div class="panel-body">
					<div>
						<div class="col-md-6">
							<h4>{{ $left['name'] }}</h4>
							Location: {{ $left['street'] }} {{ $left['city'] }}<br>
							@if (count($left['measurements']) > 0)
								Temperature <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['temperature'] }}&deg;C</span><br>
								Humidity <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['humidity'] }}&#37;</span><br>
								Light intensity <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['lightIntensity'] }}&#37;</span><br>
								Wind speed <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['windSpeed'] }}kmh</span><br>
								Wind direction <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['windDirection'] }}&deg;</span><br>
								Particulates <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['particulates'] }}&#37;</span><br>
								Carbon Monoxide <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['carbonMonoxide'] }}&#37;</span><br>
								Nitrogen Oxides <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['nitrogenOxides'] }}&#37;</span><br>
								Sulfur Oxides <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['sulfurOxides'] }}&#37;</span><br>
								Ammonia <span class="badge">{{ $left['measurements'][count($left['measurements'])-1]['ammonia'] }}&#37;</span><br>
							@endif
							
							<div class="media"><a href="/user/{{ $left['user_id'] }}">
							  <div class="media-left">
								<img class="media-object" src="{{ json_decode($left['user']['params'])->picture }}" alt="Profilepicture">
							  </div>
							  <div class="media-body">
								<h4 class="media-heading">{{ $left['user']['name'] }} <small>Owner</small></h4>
							  </div>
							</a></div>
						</div>
						<div class="col-md-6">
							<h4>{{ $right['name'] }}</h4>
							Location: {{ $right['street'] }} {{ $right['city'] }}<br>
							@if (count($right['measurements']) > 0)
								Temperature <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['temperature'] }}&deg;C</span><br>
								Humidity <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['humidity'] }}&#37;</span><br>
								Light intensity <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['lightIntensity'] }}&#37;</span><br>
								Wind speed <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['windSpeed'] }}kmh</span><br>
								Wind direction <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['windDirection'] }}&deg;</span><br>
								Particulates <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['particulates'] }}&#37;</span><br>
								Carbon Monoxide <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['carbonMonoxide'] }}&#37;</span><br>
								Nitrogen Oxides <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['nitrogenOxides'] }}&#37;</span><br>
								Sulfur Oxides <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['sulfurOxides'] }}&#37;</span><br>
								Ammonia <span class="badge">{{ $right['measurements'][count($right['measurements'])-1]['ammonia'] }}&#37;</span><br>
							@endif
							
							<div class="media"><a href="/user/{{ $left['user_id'] }}">
							  <div class="media-left">
								<img class="media-object" src="{{ json_decode($left['user']['params'])->picture }}" alt="Profilepicture">
							  </div>
							  <div class="media-body">
								<h4 class="media-heading">{{ $left['user']['name'] }} <small>Owner</small></h4>
							  </div>
							</a></div>
						</div>
					</div>
				</div>
			</div>
			
			<div class="panel panel-default">
				<div class="panel-heading">
				  <h2 class="panel-title">Graphs</h2>
				</div>
				<div class="panel-body">
					<div style="margin-bottom: -50px;" class="chart-legend pull-right"></div>
					<div><canvas id="myChart" height="350px" width="100%"></canvas></div>
					<div style="text-align:center;">
						<button type="button" class="btn btn-default prev-btn"><span class="glyphicon glyphicon-chevron-left"></span> Previous</button>
						<div class="btn-group" role="group">
						  <button type="button" class="btn btn-default graph-btn btn-primary" value="temperature">Temperature</button>
						  <button type="button" class="btn btn-default graph-btn" value="humidity">Humidity</button>
						  <button type="button" class="btn btn-default graph-btn" value="lightIntensity">Light intensity</button>
						  <button type="button" class="btn btn-default graph-btn" value="windSpeed">Wind speed</button>
						  <button type="button" class="btn btn-default graph-btn" value="particulates">Particulates</button>
						  <button type="button" class="btn btn-default graph-btn" value="carbonMonoxide">Carbon Monoxide</button>
						  <button type="button" class="btn btn-default graph-btn" value="sulfurOxides">Sulfur Oxides</button>
						  <button type="button" class="btn btn-default graph-btn" value="ammonia">Ammonia</button>
						</div>	
						<button type="button" class="btn btn-default next-btn">Next <span class="glyphicon glyphicon-chevron-right"></span></button>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection
