@extends('app')

@section('includes')
<script src="/js/ol-debug.js" type="text/javascript"></script>
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
		
			<div class="panel panel-default">
				<div class="panel-heading"><h2 class="panel-title">Add new story</h2></div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="/story">
					  <div class="form-group">
						<label for="title" class="col-sm-2 control-label">Title</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="title" placeholder="Title" name="title" value="{{ old('title') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="text" class="col-sm-2 control-label">Text</label>
						<div class="col-sm-10">
						  <textarea class="form-control" id="text" placeholder="Text" name="text" value="{{ old('text') }}" required></textarea>
						</div>
					  </div>
					  
					  <div class="form-group">
						<div class="col-sm-2">
						  Click on the location of your story
						</div>
						<div class="col-sm-10">
						  <div id="map" class="map" style="width: 100%; height: 300px;">
							<div id="popup" class="ol-popup well">
								<a href="#" id="popup-closer" class="ol-popup-closer"><span class="glyphicon glyphicon-remove pull-right"></span></a>
								<div id="popup-content"></div>
							</div>
						  </div>
						</div>
					  </div>
					  
					  <script>
					  
$( document ).ready(function() {

	/**
	 * Elements that make up the popup.
	 */
	var container = document.getElementById('popup');
	var content = document.getElementById('popup-content');
	var closer = document.getElementById('popup-closer');


	/**
	 * Add a click handler to hide the popup.
	 * @return {boolean} Don't follow the href.
	 */
	closer.onclick = function() {
	  overlay.setPosition(undefined);
	  closer.blur();
	  return false;
	};


	/**
	 * Create an overlay to anchor the popup to the map.
	 */
	var overlay = new ol.Overlay(/** @type {olx.OverlayOptions} */ ({
	  element: container,
	  autoPan: true,
	  autoPanAnimation: {
		duration: 250
	  }
	}));

	// creating the view
	var view = new ol.View({
	  center: ol.proj.transform([5.3945879493517, 52.1567739], 'EPSG:4326', 'EPSG:3857'),
	  zoom: 14
	});

	/**
	 * Create the map.
	 */
	var map = new ol.Map({
	  layers: [
		new ol.layer.Tile({
		  source: new ol.source.OSM()
		})
	  ],
	  overlays: [overlay],
	  target: 'map',
	  view: view,
	});


	/**
	 * Add a click handler to the map to render the popup.
	 */
	map.on('singleclick', function(evt) {
	  var coordinate = evt.coordinate;
	  var hdms = ol.coordinate.toStringHDMS(ol.proj.transform(
		  coordinate, 'EPSG:3857', 'EPSG:4326'));

	  content.innerHTML = '<p>You clicked here:</p><code>' + hdms +
		  '</code>';
	  overlay.setPosition(coordinate);
	  var mapcoords = ol.proj.transform(coordinate, 'EPSG:3857', 'EPSG:4326')
	  $('#lat').val(mapcoords[1]);
	  $('#lng').val(mapcoords[0]);
	  $('#submit').removeAttr("disabled");
	});


});
					  </script>
					  
					  
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <input type="hidden" name="lat" id="lat" value="0">
					  <input type="hidden" name="lng" id="lng" value="0">
					  <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						  <button type="submit" id="submit" class="btn btn-primary" disabled>Add</button> <a href="/story"><button type="button" class="btn btn-default">Cancel</button></a>
						</div>
					  </div>
					</form>
				</div>
			</div>
			
		</div>
	</div>
</div>
@endsection
