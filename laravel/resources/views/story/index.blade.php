@extends('app')

@section('content')
<div class="container">

	<div class="page-header">
	  <h1>Your stories
	  <div class="btn-group pull-right" role="group">
		<a href="/story/create"><button type="button" class="btn btn-default">Add new story</button></a>
	  </div>
	  </h1>
	</div>
	
	@if (count($ownStories) > 0)
	<div class="row">
		@foreach ($ownStories as $story)
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
				  <h3 class="panel-title">
					<a href="/story/{{ $story->id }}">{{ $story->title }}</a>
					<small class="pull-right text-muted">{{ $story->updated_at }}</small>
				  </h3>
				</div>
				<div class="panel-body">
				{{ $story->text }}
				</div>
			</div>
		</div>
		@endforeach
	</div>
	@else
	<div class="alert alert-info" role="alert"><strong>You do not have any stories yet!</strong> You can add one by pressing the &quot;Add new story&quot; button above.</div>
	@endif
	
	<div class="page-header">
	  <h1>All stories
	  <form class="form-inline pull-right" action="/story/search" method="get">
		<div class="input-group">
		  <input type="search" name="q" class="form-control" autofocus>
		  <span class="input-group-btn">
			<button class="btn btn-default" type="submit">
			  Search
			</button>
		  </span>
		</div>
	  </form>
	  </h1>
	</div>
	
	@if (count($stories) > 0)
	<div class="row">
		@foreach ($stories as $story)
		<div class="col-md-12">
			<div class="panel panel-default">
				<div class="panel-heading">
				  <h3 class="panel-title">
					<a href="/story/{{ $story->id }}">{{ $story->title }}</a>
					<small class="pull-right text-muted">{{ $story->updated_at }}</small>
				  </h3>
				</div>
				<div class="panel-body">
				{{ $story->text }}
				</div>
			</div>
		</div>
		@endforeach
	</div>
	@else
		<div class="alert alert-info" role="alert"><strong>No stories found!</strong> That's a pitty. You can add one by pressing the &quot;Add new story&quot; button above.</div>
	@endif
	
</div>
@endsection
