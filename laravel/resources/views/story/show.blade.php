@extends('app')

@section('content')
<div class="container">
	
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading">
				  <h2 class="panel-title">{{ $story->title }}<small class="pull-right text-muted">{{ $story->updated_at }}</small></h2>
				</div>
				
	@if (count($story->images) > 0)		
			<div id="carousel" class="carousel slide">
			  <!-- Indicators -->
			  <ol class="carousel-indicators">
				@for ($i = 0; $i < count($story->images); $i++)
					@if ($i == 0)
					<li onclick="$('.carousel').carousel({{ $i }})" class="active"></li>
					@else
					<li onclick="$('.carousel').carousel({{ $i }})"></li>
					@endif
				@endfor
			  </ol>

			  <!-- Wrapper for slides -->
			  <div class="carousel-inner" role="listbox">
			  @foreach ($story->images as $key => $image)
				@if ($key == 0)
				<div class="item active">
				@else
				<div class="item">
				@endif
				  <img src="{{ asset('image/'.$image->id) }}" alt="{{ $image->caption or '' }}" style="margin-left: auto; margin-right: auto;">
				  <div class="carousel-caption">
				  {{ $image->caption or '' }}
				  </div>
				</div>
			  @endforeach
			  </div>

			  <!-- Controls -->
			  <a class="left carousel-control" href="#carousel" role="button" onclick="$('.carousel').carousel('prev')">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			  </a>
			  <a class="right carousel-control" href="#carousel" role="button" onclick="$('.carousel').carousel('next')">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			  </a>
			  <a class="carousel-fullscreen" role="button">
				<span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
				<span class="sr-only">Fullscreen</span>
			  </a>
			</div>
			<script>
				$( document ).ready(function() {
					$('.carousel').carousel({
					  interval: 7500
					})
				});
			</script>
	@endif
				
				<div class="panel-body">
					@if ($story->user_id != Auth::id())
					<div class="media"><a href="/user/{{ $story->user_id }}">
					  <div class="media-left">
						<img class="media-object" src="{{ json_decode($story->user->params)->picture }}" alt="Profilepicture">
					  </div>
					  <div class="media-body">
						<h4 class="media-heading">{{ $story->user->name }} <small>Author</small></h4>
					  </div>
					</a></div>
					@endif
					
					{{ $story->text }}

					@if ($story->user_id == Auth::id())
					<span class="btn-group pull-right" role="group">
					  <a href="/story/{{ $story->id }}/edit"><button type="button" class="btn btn-default">Edit</button></a>
					</span>
					@endif
				</div>
				
				<ul class="list-group">
				@if (count($story->comments) > 0)
					@foreach ($story->comments as $comment)
					<li class="list-group-item"><strong>{{ $comment->user->name }}:</strong> {{ $comment->text }} <small class="pull-right text-muted">{{ $comment->updated_at }}</small></li>
					@endforeach
				@endif
					<li class="list-group-item">
						<form class="form-horizontal" role="form" method="POST" action="/comment">
						  <div class="form-group">
							<div class="col-sm-10 col-xs-8">
							  <input type="text" class="form-control" id="text" placeholder="Add comment" name="text" value="{{ old('text') }}" required>
							</div>
							<div class="col-sm-2 col-xs-4">
							  <button type="submit" id="submit" class="btn btn-primary">Comment</button>
							</div>
						  </div>
						  <input type="hidden" name="_token" value="{{ csrf_token() }}">
						  <input type="hidden" name="model" id="model" value="story">
						  <input type="hidden" name="id" id="id" value="{{ $story->id }}">
						</form>
					</li>
				</ul>
				
			</div>
			
		</div>
	</div>
</div>
@endsection
