@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-12">
			
			<div class="panel panel-default">
				<div class="panel-heading"><h2 class="panel-title">Edit story</h2></div>
				<div class="panel-body">
					<form class="form-horizontal" role="form" method="POST" action="/story/{{ $story->id }}">
					
					@if (count($messages) > 0)
						<div class="alert alert-danger">
							<strong>Whoops!</strong> There were some problems with your input.<br><br>
							<ul>
								@foreach ($messages as $message)
									<li>{{ $message }}</li>
								@endforeach
							</ul>
						</div>
						
					  <div class="form-group">
						<label for="title" class="col-sm-2 control-label">Title</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="title" placeholder="Title" name="title" value="{{ old('title') }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="text" class="col-sm-2 control-label">Text</label>
						<div class="col-sm-10">
						  <textarea class="form-control" id="text" placeholder="Text" name="text" required>{{ old('text') }}</textarea>
						</div>
					  </div>
					  
					@else
						
					 <div class="form-group">
						<label for="title" class="col-sm-2 control-label">Title</label>
						<div class="col-sm-10">
						  <input type="text" class="form-control" id="title" placeholder="Title" name="title" value="{{ $story->title }}" required>
						</div>
					  </div>
					  <div class="form-group">
						<label for="text" class="col-sm-2 control-label">Text</label>
						<div class="col-sm-10">
						  <textarea class="form-control" id="text" placeholder="Text" name="text" required>{{ $story->text }}</textarea>
						</div>
					  </div>
					  
					@endif
					  
					  <input type="hidden" name="_token" value="{{ csrf_token() }}">
					  <input type="hidden" name="_method" value="PUT">
					  <div class="form-group">
						<div class="col-sm-offset-2 col-sm-10">
						  <button type="submit" id="submit" class="btn btn-primary">Save</button> <a href="/story/{{ $story->id }}"><button type="button" class="btn btn-default">Cancel</button></a>
						</div>
					  </div>
					</form>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
