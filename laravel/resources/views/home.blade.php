@extends('app')

@section('includes')
<link href="/css/special.css" rel="stylesheet">
@endsection

@section('content')
<div id="map" class="map"><div id="popup"></div></div>
<div id="info" style="display: none;"></div>
<img id="geolocation_marker" src="/images/marker.png" />
<div class="button">
	<button id="expand" class="btn btn-default" onclick="$('.mapsettings').collapse('toggle');"><span class="glyphicon glyphicon-cog"></span></button>
	<button id="geolocate" class="btn btn-primary">Find me!</button>
	<br><br>
	<div class="well well-sm collapse mapsettings">
	  <div class="btn-group layer-btn-group" role="group">
		<button type="button" class="btn layer-btn btn-primary" value="heatmap"><span class="glyphicon glyphicon-file"></span> Heatmap Layer</button>
		<button type="button" class="btn layer-btn btn-primary" value="station"><span class="glyphicon glyphicon-map-marker"></span> Stations Layer</button>
		<button type="button" class="btn layer-btn btn-primary" value="story"><span class="glyphicon glyphicon-comment"></span> Stories Layer</button>
	  </div>
	  <form>
		<label>radius size</label>
		<input id="radius" type="range" min="1" max="100" step="1" value="50"/>
		<label>blur size</label>
		<input id="blur" type="range" min="1" max="50" step="1" value="25"/>
	  </form>
	  <div class="heatmap-btn-group">
		  <div class="btn-group" role="group">
			<button type="button" class="btn btn-default heatmap-btn btn-primary" value="temperature">Temperature</button>
			<button type="button" class="btn btn-default heatmap-btn" value="humidity">Humidity</button>
			<button type="button" class="btn btn-default heatmap-btn" value="lightIntensity">Light intensity</button>
			<button type="button" class="btn btn-default heatmap-btn" value="windSpeed">Wind speed</button>
		  </div>
		  <br>
		  <div class="btn-group" role="group">
			<button type="button" class="btn btn-default heatmap-btn" value="particulates">Particulates</button>
			<button type="button" class="btn btn-default heatmap-btn" value="carbonMonoxide">Carbon Monoxide (CO)</button>
			<button type="button" class="btn btn-default heatmap-btn" value="ammonia">Ammonia (NH<sub>3</sub> )</button>
		  </div>
		  <br>
		  <div class="btn-group" role="group">
			<button type="button" class="btn btn-default heatmap-btn" value="nitrogenOxides">Nitrogen Oxides (NO<sub>x</sub> )</button>
			<button type="button" class="btn btn-default heatmap-btn" value="sulfurOxides">Sulfur Oxides (SO<sub>x</sub> )</button>
		  </div>
	  </div>
	  <div class="well well-sm" id="gradient">
		<span id="min" class="badge"></span>
		<span id="max" class="badge pull-right"></span>
	  </div>
	</div>
</div>
<script src="/js/ol-debug.js" type="text/javascript"></script>
<script src="/js/geolocation-orientation.js" type="text/javascript"></script>
@endsection
