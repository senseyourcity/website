$( document ).ready(function() {
	
	// create station marker
	var iconFeature = new Array();
	var heatmapFeature = new Array();
	var storyFeature = new Array();

	var iconStyle = new ol.style.Style({
	  image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
		anchor: [0.5, 1],
		anchorXUnits: 'fraction',
		anchorYUnits: 'fraction',
		opacity: 0.75,
		scale: .15,
		src: 'images/marker1.png'
	  }))
	});
	
	var storyStyle = new ol.style.Style({
	  image: new ol.style.Icon(/** @type {olx.style.IconOptions} */ ({
		anchor: [0.5, 1],
		anchorXUnits: 'fraction',
		anchorYUnits: 'fraction',
		opacity: 0.75,
		scale: .15,
		src: 'images/marker.png'
	  }))
	});

	// set vectorsource and layer
	var vectorSource = new ol.source.Vector({
	  features: iconFeature
	});

	var vectorLayer = new ol.layer.Vector({
	  source: vectorSource
	});
	
	// set storysource and layer
	var storySource = new ol.source.Vector({
	  features: storyFeature
	});

	var storyLayer = new ol.layer.Vector({
	  source: storySource
	});

	var blur = $('#blur');
	var radius = $('#radius');
	var heatmapType = 'temperature';
	var temperatureGradient = ['#00f', '#0ff', '#0f0', '#ff0', '#f00'];
	var humidityGradient = ['#fff','#aff','#0ff','#0af','#00f'];
	var lightIntensityGradient = ['#000','#f00','#fa0','#ff0','#ffa'];
	var particulatesGradient = ['#fff','#ccc','#aaa','#666','#000'];
	var carbonMonoxideGradient = ['#fff','#ccc','#aaa','#666','#000'];
	var nitrogenOxidesGradient = ['#fff','#cfc','#afa','#6f6','#0f0'];
	var sulfurOxidesGradient = ['#fff','#ffc','#ffa','#ff6','#ff0'];
	var ammoniaGradient = ['#fff','#ccc','#aaa','#666','#000'];
	var temperatureRange = [0,10];
	var humidityRange = [0,100];
	var lightIntensityRange = [0,10];
	var windSpeedRange = [0,10];
	var particulatesRange = [0,10];
	var carbonMonoxideRange = [0,10];
	var nitrogenOxidesRange = [0,10];
	var sulfurOxidesRange = [0,10];
	var ammoniaRange = [0,10];
	
	function detectGradient(gradientValue) {
		//var gradientValue = $('input[type=radio][name=heatmap]:checked').val();
		if (gradientValue === 'humidity'){
			return humidityGradient;
		} else if (gradientValue === 'lightIntensity'){
			return lightIntensityGradient;
		} else if (gradientValue === 'particulates'){
			return particulatesGradient;
		} else if (gradientValue === 'carbonMonoxide'){
			return carbonMonoxideGradient;
		} else if (gradientValue === 'nitrogenOxides'){
			return nitrogenOxidesGradient;
		} else if (gradientValue === 'sulfurOxides'){
			return sulfurOxidesGradient;
		} else if (gradientValue === 'ammonia'){
			return ammoniaGradient;
		} else {
			return temperatureGradient;
		}
	}
	
	function setGradientDisplay(gradient) {
		$('#gradient').css({
			background: "linear-gradient(to right, "+gradient[0]+","+gradient[1]+","+gradient[2]+","+gradient[3]+","+gradient[4]+")"}).css({
			background: "-moz-linear-gradient(left, "+gradient[0]+","+gradient[1]+","+gradient[2]+","+gradient[3]+","+gradient[4]+")"});
	}
	
	setGradientDisplay(detectGradient());
	
	function detectRange(rangeValue) {
		//var rangeValue = $('input[type=radio][name=heatmap]:checked').val();
		if (rangeValue === 'humidity'){
			return humidityRange;
		} else if (rangeValue === 'temperature') {
			return temperatureRange;
		} else if (rangeValue === 'windSpeed') {
			return windSpeedRange;
		} else if (rangeValue === 'lightIntensity') {
			return lightIntensityRange;
		} else if (rangeValue === 'particulates') {
			return particulatesRange;
		} else if (rangeValue === 'carbonMonoxide') {
			return carbonMonoxideRange;
		} else if (rangeValue === 'nitrogenOxides') {
			return nitrogenOxidesRange;
		} else if (rangeValue === 'sulfurOxides') {
			return sulfurOxidesRange;
		} else if (rangeValue === 'ammonia') {
			return ammoniaRange;
		} else {
			return [0,100];
		}
	}
	
	function setRangeDisplay(range) {
		$('#min').text(range[0]);
		$('#max').text(range[1]);
	}

	// set heatmapsource and layer
	var heatmapSource = new ol.source.Vector({
	  features: heatmapFeature
	});
	
	var heatmapLayer = new ol.layer.Heatmap({
	  source: heatmapSource,
	  blur: parseInt(blur.val(), 10),
	  radius: parseInt(radius.val(), 10),
	  weight: 'temperature',
	  gradient: detectGradient()
	});
	
	// heatmap controls
	blur.on('input', function() {
	  heatmapLayer.setBlur(parseInt(blur.val(), 10));
	});

	radius.on('input', function() {
	  heatmapLayer.setRadius(parseInt(radius.val(), 10));
	});

	// creating the view
	var view = new ol.View({
	  center: ol.proj.transform([5.3965879493517, 52.1637739], 'EPSG:4326', 'EPSG:3857'),
	  zoom: 14
	});

	// creating the map
	var map = new ol.Map({
	  layers: [
		new ol.layer.Tile({
		  source: new ol.source.OSM()
		}),
		heatmapLayer,vectorLayer,storyLayer
	  ],
	  target: 'map',
	  controls: ol.control.defaults({
		attributionOptions: /** @type {olx.control.AttributionOptions} */ ({
		  collapsible: false
		})
	  }).extend([
		new ol.control.FullScreen(),
		new ol.control.ZoomSlider()
	  ]),
	  view: view,
	});
	
	$( ".heatmap-btn" ).click(function() {
		var type = $(this).attr("value");
		map.removeLayer(heatmapLayer);
		var visible = heatmapLayer.getVisible();
	    heatmapLayer = new ol.layer.Heatmap({
		  source: heatmapSource,
		  blur: parseInt(blur.val(), 10),
		  radius: parseInt(radius.val(), 10),
		  weight: type,
		  gradient: detectGradient(type)
		});
		heatmapLayer.setVisible(visible);
		map.addLayer(heatmapLayer);
		map.removeLayer(vectorLayer);
		map.addLayer(vectorLayer);
		map.removeLayer(storyLayer);
		map.addLayer(storyLayer);
		setGradientDisplay(detectGradient(type));
		setRangeDisplay(detectRange(type));
		$( ".heatmap-btn" ).removeClass('btn-primary');
		$(this).addClass('btn-primary');
	});
	
	$( ".layer-btn" ).click(function() {
		var type = $(this).attr("value");
		if ($(this).hasClass('btn-primary')){
			$(this).removeClass('btn-primary');
			$(this).addClass('btn-default');
			if (type == 'heatmap') heatmapLayer.setVisible(false);
			if (type == 'station') vectorLayer.setVisible(false);
			if (type == 'story') storyLayer.setVisible(false);
		} else {
			$(this).removeClass('btn-default');
			$(this).addClass('btn-primary');
			if (type == 'heatmap') heatmapLayer.setVisible(true);
			if (type == 'station') vectorLayer.setVisible(true);
			if (type == 'story') storyLayer.setVisible(true);
		}
	});
	
	// ajax request for measurement data
	function getData() {
	$.getJSON( "data/latest/measurement", function( data ) {
		iconFeature = new Array();
		heatmapFeature = new Array();
		$.each( data.data, function( key, object ) {
			var newDatapoint = new ol.Feature({
			  geometry: new ol.geom.Point(ol.proj.transform([object.longitude, object.latitude], 'EPSG:4326', 'EPSG:3857')),
			  name: object.name,
			  id: object.id,
			  type: 'heatmapFeature',
			  temperature: (object.measurement.temperature - data.temperatureRange[0]) / (data.temperatureRange[1]-data.temperatureRange[0]-0.1) + 0.1,
			  humidity: (object.measurement.humidity - data.humidityRange[0]) / (data.humidityRange[1]-data.humidityRange[0]-0.1) + 0.1,
			  lightIntensity: object.measurement.lightIntensity * 0.1,
			  windSpeed: (object.measurement.windSpeed - data.windSpeedRange[0]) / (data.windSpeedRange[1]-data.windSpeedRange[0]-0.1) + 0.1,
			  windDirection: object.measurement.windDirection,
			  particulates: object.measurement.particulates * 0.1,
			  carbonMonoxide: object.measurement.carbonMonoxide * 0.1,
			  nitrogenOxides: object.measurement.nitrogenOxides * 0.1,
			  sulfurOxides: object.measurement.sulfurOxides * 0.1,
			  ammonia: object.measurement.ammonia * 0.1,
			  weight: object.measurement.temperature* 0.01,
			});
			heatmapFeature.push(newDatapoint);
			var newIcon = new ol.Feature({
			  geometry: new ol.geom.Point(ol.proj.transform([object.longitude, object.latitude], 'EPSG:4326', 'EPSG:3857')),
			  name: object.name,
			  id: object.id,
			  type: 'iconFeature',
			  temperature: object.measurement.temperature,
			  humidity: object.measurement.humidity,
			  lightIntensity: object.measurement.lightIntensity,
			  windSpeed: object.measurement.windSpeed,
			  windDirection: object.measurement.windDirection,
			  particulates: object.measurement.particulates,
			  carbonMonoxide: object.measurement.carbonMonoxide,
			  nitrogenOxides: object.measurement.nitrogenOxides,
			  sulfurOxides: object.measurement.sulfurOxides,
			  ammonia: object.measurement.ammonia,
			});
			newIcon.setStyle(iconStyle);
			iconFeature.push(newIcon);
		});
		vectorSource.clear();
		vectorSource.addFeatures(iconFeature);
		heatmapSource.clear();
		heatmapSource.addFeatures(heatmapFeature);
		temperatureRange = data.temperatureRange;
		humidityRange = data.humidityRange;
		windSpeedRange = data.windSpeedRange;
		setRangeDisplay(detectRange());
	});
	}
	
	getData();
	window.setInterval(function () {getData();}, 60000);
	
	// ajax request for story data
	$.getJSON( "data/story", function( data ) {
		$.each( data.data, function( key, object ) {
			var image = 1;
			if (object.images.length > 0){
				image = object.images[object.images.length-1].id;
			}
			var newStory = new ol.Feature({
			  geometry: new ol.geom.Point(ol.proj.transform([object.lng, object.lat], 'EPSG:4326', 'EPSG:3857')),
			  title: object.title,
			  id: object.id,
			  type: 'storyFeature',
			  text: object.text.substring(0, 100),
			  image: image,
			});
			newStory.setStyle(storyStyle);
			storyFeature.push(newStory);
		});
		storySource.clear();
		storySource.addFeatures(storyFeature);
	});
	
	// popup
	var element = document.getElementById('popup');

	var popup = new ol.Overlay({
	  element: element,
	  positioning: 'bottom-center',
	  stopEvent: false
	});
	map.addOverlay(popup);
	var popupVisible = false;

	// display popup on click
	map.on('click', function(evt) {
	  var feature = map.forEachFeatureAtPixel(evt.pixel,
		  function(feature, layer) {
			return feature;
		  }, null, function(layer) {
			return (layer === vectorLayer || layer === storyLayer);
		  });
	  if (feature) {
		if (popupVisible){
			$(element).popover('destroy');
			popupVisible = false;
		} else {
			var geometry = feature.getGeometry();
			var coord = geometry.getCoordinates();
			popup.setPosition(coord);
			if (feature.get('type') == 'iconFeature'){
				$(element).popover({
				  'placement': 'top',
				  'html': true,
				  'title': '<a href="stations/'+feature.get('id')+'">'+feature.get('name')+'</a>',
				  'content': '<strong class="col-xs-4" style="display: inline-block">Measurements</strong><br>'
							+"Temperature: "+feature.get('temperature')
							+"<br>Humidity: "+feature.get('humidity')
							+"<br>Light intensity: "+feature.get('lightIntensity')
							+"<br>Wind speed: "+feature.get('windSpeed')
							+"<br>Wind direction: "+feature.get('windDirection')
							+"<br>Particulates: "+feature.get('particulates')
							+"<br>Carbon Monoxide: "+feature.get('carbonMonoxide')
							+"<br>Nitrogen Oxides: "+feature.get('nitrogenOxides')
							+"<br>Sulfur Oxides: "+feature.get('sulfurOxides')
							+"<br>Ammonia: "+feature.get('ammonia')
				});
			} else if (feature.get('type') == 'storyFeature'){
				$(element).popover({
				  'placement': 'top',
				  'html': true,
				  'title': '<a href="story/'+feature.get('id')+'">'+feature.get('title')+'</a>',
				  'content': '<img width="200" src="image/'+feature.get('image')+'"><br>'
							+feature.get('text')+'...',
				});
			}
			$(element).popover('show');
			popupVisible = true;
		}
	  } else {
		$(element).popover('destroy');
		popupVisible = false;
	  }
	});

	// change mouse cursor when over marker
	map.on('pointermove', function(e) {
	  if (e.dragging) {
		$(element).popover('destroy');
		popupVisible = false;
		return;
	  }
	  var pixel = map.getEventPixel(e.originalEvent);
	  //var hit = map.hasFeatureAtPixel(pixel);
	  var hit = map.forEachFeatureAtPixel(pixel,
		  function(feature, layer) {
			return feature;
		  }, null, function(layer) {
			return (layer === vectorLayer || layer === storyLayer);
		  });
	  document.getElementById('map').style.cursor = hit ? 'pointer' : '';
	});

	// Geolocation marker
	var markerEl = document.getElementById('geolocation_marker');
	var marker = new ol.Overlay({
	  positioning: 'center-center',
	  element: markerEl,
	  stopEvent: false
	});
	map.addOverlay(marker);

	// LineString to store the different geolocation positions. This LineString
	// is time aware.
	// The Z dimension is actually used to store the rotation (heading).
	var positions = new ol.geom.LineString([],
		/** @type {ol.geom.GeometryLayout} */ ('XYZM'));

	// Geolocation Control
	var geolocation = new ol.Geolocation(/** @type {olx.GeolocationOptions} */ ({
	  projection: view.getProjection(),
	  trackingOptions: {
		maximumAge: 10000,
		enableHighAccuracy: true,
		timeout: 600000
	  }
	}));

	var deltaMean = 500; // the geolocation sampling period mean in ms

	// Listen to position changes
	geolocation.on('change', function(evt) {
	  var position = geolocation.getPosition();
	  var accuracy = geolocation.getAccuracy();
	  var heading = geolocation.getHeading() || 0;
	  var speed = geolocation.getSpeed() || 0;
	  var m = Date.now();

	  addPosition(position, heading, m, speed);

	  var coords = positions.getCoordinates();
	  var len = coords.length;
	  if (len >= 2) {
		deltaMean = (coords[len - 1][3] - coords[0][3]) / (len - 1);
	  }
	});

	geolocation.on('error', function() {
	  alert('geolocation error');
	  geolocation.setTracking(false);
	  map.un('postcompose', render);
	  view.setZoom(14);
	  $('#geolocate').text('Find me!');
	  // FIXME we should remove the coordinates in positions
	});

	// convert radians to degrees
	function radToDeg(rad) {
	  return rad * 360 / (Math.PI * 2);
	}
	// convert degrees to radians
	function degToRad(deg) {
	  return deg * Math.PI * 2 / 360;
	}
	// modulo for negative values
	function mod(n) {
	  return ((n % (2 * Math.PI)) + (2 * Math.PI)) % (2 * Math.PI);
	}

	function addPosition(position, heading, m, speed) {
	  var x = position[0];
	  var y = position[1];
	  var fCoords = positions.getCoordinates();
	  var previous = fCoords[fCoords.length - 1];
	  var prevHeading = previous && previous[2];
	  if (prevHeading) {
		var headingDiff = heading - mod(prevHeading);

		// force the rotation change to be less than 180°
		if (Math.abs(headingDiff) > Math.PI) {
		  var sign = (headingDiff >= 0) ? 1 : -1;
		  headingDiff = - sign * (2 * Math.PI - Math.abs(headingDiff));
		}
		heading = prevHeading + headingDiff;
	  }
	  positions.appendCoordinate([x, y, heading, m]);

	  // only keep the 20 last coordinates
	  positions.setCoordinates(positions.getCoordinates().slice(-20));

	  // FIXME use speed instead
	  if (heading && speed) {
		markerEl.src = 'images/marker.png';
	  } else {
		markerEl.src = 'images/marker.png';
	  }
	}

	var previousM = 0;
	// change center and rotation before render
	map.beforeRender(function(map, frameState) {
	  if (frameState !== null && geolocation.getTracking()) {
		// use sampling period to get a smooth transition
		var m = frameState.time - deltaMean * 1.5;
		m = Math.max(m, previousM);
		previousM = m;
		// interpolate position along positions LineString
		var c = positions.getCoordinateAtM(m, true);
		var view = frameState.viewState;
		if (c) {
		  view.center = getCenterWithHeading(c, -c[2], view.resolution);
		  view.rotation = -c[2];
		  marker.setPosition(c);
		}
	  }
	  return true; // Force animation to continue
	});

	// recenters the view by putting the given coordinates at 3/4 from the top or
	// the screen
	function getCenterWithHeading(position, rotation, resolution) {
	  var size = map.getSize();
	  var height = size[1];

	  return [
		position[0] - Math.sin(rotation) * height * resolution * 1 / 4,
		position[1] + Math.cos(rotation) * height * resolution * 1 / 4
	  ];
	}

	// postcompose callback
	function render() {
	  map.render();
	}

	// geolocate device
	var geolocateBtn = document.getElementById('geolocate');
	geolocateBtn.addEventListener('click', function() {
	  if(!geolocation.getTracking()){
		  geolocation.setTracking(true); // Start position tracking
		  view.setZoom(17);

		  map.on('postcompose', render);
		  map.render();
		  $('#geolocate').text('Stop locating');
	  } else {
		  geolocation.setTracking(false);
		  map.un('postcompose', render);
		  view.setZoom(14);
		  $('#geolocate').text('Find me!');
	  }
	}, false);

});